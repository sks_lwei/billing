package com.hmkj.crm.billing.domain.company;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.hmkj.crm.billing.domain.BaseDomain;
import com.hmkj.crm.billing.dto.SelectItem;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * 供应商及企业信息
 *
 * 维护供应商以及生产企业信息
 *
 * @author sks.lwei
 * @date 2020/01/16
 */
@Getter
@Setter
@TableName("supplier_info")
@ToString(callSuper = true)
public class SupplierInfo extends BaseDomain<SupplierInfo> implements SelectItem {

    /**
     * 通用
     */
    public static final Integer NORMAL = 0;

    /**
     * 供应商
     */
    public static final Integer SUPPLY = 1;
    /**
     * 生产企业
     */
    public static final Integer PRODUCT = 2;

    /**
     * 类型
     *
     * 供应商 or 生产企业 or 通用
     * 1 2 0
     * 默认为0
     */
    @TableField
    private Integer type;

    /**
     * 全名
     */
    @TableField("full_name")
    private String fullName;

    /**
     * 备注
     */
    @TableField
    private String remark;

    @Override
    public String getText() {
        return getFullName();
    }

    @Override
    public Object getVal() {
        return getUuid();
    }
}
