package com.hmkj.crm.billing.service.order.impl;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hmkj.crm.billing.domain.company.DeliveryCompaniesInfo;
import com.hmkj.crm.billing.domain.order.PurchaseOrder;
import com.hmkj.crm.billing.domain.order.SalesOrder;
import com.hmkj.crm.billing.domain.product.ProductInfo;
import com.hmkj.crm.billing.dto.PurchasePriceDTO;
import com.hmkj.crm.billing.dto.SalesOrderDTO;
import com.hmkj.crm.billing.enums.AmountTypeEnum;
import com.hmkj.crm.billing.export.ExportComponent;
import com.hmkj.crm.billing.export.domain.SalesOrderExport;
import com.hmkj.crm.billing.mapper.order.SalesOrderMapper;
import com.hmkj.crm.billing.service.company.DeliveryCompaniesInfoService;
import com.hmkj.crm.billing.service.company.InventoryDetailService;
import com.hmkj.crm.billing.service.order.PurchaseOrderService;
import com.hmkj.crm.billing.service.order.SalesOrderService;
import com.hmkj.crm.billing.service.product.ProductInfoService;
import com.hmkj.crm.billing.service.relevance.RelProductInventoryService;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 销售订单(出库)服务Impl
 *
 * @author sks.lwei
 * @date 2020/01/16
 */
@Log4j2
@Service
public class SalesOrderServiceImpl extends ServiceImpl<SalesOrderMapper, SalesOrder> implements SalesOrderService {

    /**
     * rel产品库存服务
     */
    @Resource
    private RelProductInventoryService productInventoryService;

    /**
     * 产品信息服务
     */
    @Resource
    private ProductInfoService productInfoService;

    /**
     * 配送公司信息服务
     */
    @Resource
    private DeliveryCompaniesInfoService deliveryCompaniesInfoService;

    /**
     * 购买订单服务
     */
    @Resource
    private PurchaseOrderService purchaseOrderService;

    /**
     * 公司账户，账单 服务
     */
    @Resource
    private InventoryDetailService inventoryDetailService;

    @Override
    public IPage<SalesOrderDTO> listSalesOrderPage(Page<SalesOrder> page, Wrapper<SalesOrder> wrapper) {
        Page<SalesOrder> orderPage = this.page(page, wrapper);
        return orderPage.convert(salesOrder -> {
            SalesOrderDTO dto = new SalesOrderDTO(salesOrder);
            ProductInfo productInfo = productInfoService.getProductInfoByUuid(salesOrder.getProductUuid());
            DeliveryCompaniesInfo deliveryCompaniesInfo = deliveryCompaniesInfoService.getDeliveryCompaniesInfoByuuid(salesOrder.getDeliveryUuid());
            //查询每笔订单产品的进价
            PurchasePriceDTO purchaseInfo = purchaseOrderService.getPurchaseInfo(productInfo.getUuid(), deliveryCompaniesInfo.getUuid());
            dto.setTaxPaid(purchaseInfo.getTaxPaid());
            dto.setTaxAmount(dto.getTaxPaid() * salesOrder.getNumber());
            return dto.toSalesOrderDTO(productInfo, deliveryCompaniesInfo);
        });
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void sales(String deliveryUuid, SalesOrder salesOrder) {
        log.info("销售订单, 订单uuid[{}]", salesOrder.getUuid());

        salesOrder.setUuid("S"+ IdWorker.getMillisecond());
        this.save(salesOrder);

        //削减库存
        productInventoryService.down(deliveryUuid, salesOrder);
        log.info("订单出库完成");
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void unSales(Long id, String mark) {
        SalesOrder salesOrder = this.getById(id);

        if(salesOrder == null){
            log.warn("订单id[{}]不存在，撤销失败", id);
            throw new NullPointerException("订单不存在，撤销失败!");
        }
        String salesOrderUuid = salesOrder.getUuid();
        log.info("销售订单撤销, 订单uuid[{}]", salesOrderUuid);
        String deliveryUuid = salesOrder.getDeliveryUuid();

        //库存清算
        log.info("销售订单撤销，库存清算");
        productInventoryService.up(deliveryUuid, salesOrder.toPurchaseOrder());

//        log.info("执行公司账户金额清退操作");
//        inventoryDetailService.accountChange(salesOrderUuid, deliveryUuid, salesOrder.getSaleTotalPrice(), AmountTypeEnum.INCOME, "订单["+salesOrderUuid+"]撤回退款");
//        log.info("公司账户变动完成！");

        UpdateWrapper<SalesOrder> update = new UpdateWrapper<>();
        update.set("remark", mark).eq("uuid", salesOrderUuid);
        this.update(update);
        log.info("记录撤回原因[{}]", mark);

        this.removeById(id);
    }

    @Override
    public SalesOrder getSalesOrderByUuid(String uuid) {
        QueryWrapper<SalesOrder> wrapper = new QueryWrapper<>();
        wrapper.eq("uuid", uuid);
        return this.getOne(wrapper);
    }

    @Override
    public List<? extends ExportComponent> export(QueryWrapper<SalesOrder> wrapper) {
        List<SalesOrder> salesOrders = this.list(wrapper);

        return salesOrders.stream().map(salesOrder -> {
            SalesOrderExport salesOrderExport = new SalesOrderExport(salesOrder);
            ProductInfo productInfo = productInfoService.getProductInfoByUuid(salesOrder.getProductUuid());
            BeanUtil.copyProperties(productInfo, salesOrderExport);
            salesOrderExport.deliveryName = deliveryCompaniesInfoService.getDeliveryCompaniesNameByUuid(salesOrder.getDeliveryUuid());
            //查询每笔订单产品的进价
            PurchasePriceDTO purchaseInfo = purchaseOrderService.getPurchaseInfo(salesOrder.getProductUuid(), salesOrder.getDeliveryUuid());
            salesOrderExport.taxPaid = purchaseInfo.getTaxPaid();
            salesOrderExport.taxAmount = salesOrder.getNumber() * salesOrderExport.taxPaid;
            return salesOrderExport;
        }).collect(Collectors.toList());
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void del(Long id) {
        SalesOrder salesOrder = this.getById(id);
        log.info("删除销售订单, 订单uuid[{}]", salesOrder.getUuid());

        //撤销库存
        productInventoryService.undo(salesOrder.getDeliveryUuid(), salesOrder.getProductUuid(), salesOrder.getUuid());
        log.info("库存已撤销");

        this.removeById(salesOrder.getId());
        log.warn("删除销售订单完成");
    }
}
