package com.hmkj.crm.billing.service.relevance;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.hmkj.crm.billing.domain.order.Order;
import com.hmkj.crm.billing.domain.relevance.ProductInventoryDetail;
import com.hmkj.crm.billing.domain.relevance.RelProductInventory;
import com.hmkj.crm.billing.dto.ProductInventoryDetailDTO;
import com.hmkj.crm.billing.export.ExportComponent;

import java.util.List;

/**
 * 产品库存关联表明细 服务
 *
 * @author sks.lwei
 * @date 2020/02/15
 */
public interface ProductInventoryDetailService extends IService<ProductInventoryDetail> {

    /**
     * 添加产品库存关联表明细
     *
     * @param relProductInventoryUuid rel产品库存Uuid
     * @param order                   订单
     */
    void addDetail(String relProductInventoryUuid, Order order);

    /**
     * 产品库存详细信息页面列表
     *
     * @param page 页面
     * @return {@link IPage<ProductInventoryDetailDTO>}
     */
    IPage<ProductInventoryDetailDTO> listProductInventoryDetailPage(Page<ProductInventoryDetail> page, Wrapper<ProductInventoryDetail> wrapper);

    /**
     * 撤销(删除库存变更记录)
     *
     * @param inventoryUuid rel产品库存Uuid (这个产品对应的仓库记录)
     * @param orderNumber  订单号
     * @return {@link ProductInventoryDetail} 返回这条记录快照
     */
    ProductInventoryDetail undo(String inventoryUuid, String orderNumber);

    /**
     * 导出
     *
     * @param wrapper 包装器
     */
    List<? extends ExportComponent> export(QueryWrapper<ProductInventoryDetail> wrapper);
}
