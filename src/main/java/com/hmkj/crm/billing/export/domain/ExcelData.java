package com.hmkj.crm.billing.export.domain;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Excel数据 模型
 *
 * @author sks.lwei
 * @date 2020/03/02
 */
@Getter
@Setter
@ToString
public class ExcelData implements Serializable {

    private static final long serialVersionUID = 4444017239100620999L;

    /**
     * 表头
     */
    private List<String> titles;

    /**
     * 数据
     */
    private List<List<Object>> rows;

    /**
     * 页签名称
     */
    private String name;


}
