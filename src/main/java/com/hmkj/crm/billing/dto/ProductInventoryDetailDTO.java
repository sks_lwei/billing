package com.hmkj.crm.billing.dto;

import cn.hutool.core.bean.BeanUtil;
import com.hmkj.crm.billing.domain.order.ReturnOrder;
import com.hmkj.crm.billing.domain.order.SalesOrder;
import com.hmkj.crm.billing.domain.relevance.ProductInventoryDetail;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * 产品库存详细DTO
 *
 * @author sks.lwei
 * @date 2020/02/19
 */
@Getter
@Setter
@ToString
public class ProductInventoryDetailDTO extends ProductInventoryDetail {

    //库存相关字段--来自订单

    /**
     * 含税进/销价(单价)
     */
    private Double price;

    /**
     * 含税总金额(付款额)
     */
    private Double paymentAmount;

    /**
     * 不含税进/销价
     */
    private Double noTaxPaid;

    /**
     * 不含税总金额
     */
    private Double noTaxAmount;

    /**
     * 返款价格
     */
    private Double returnPrice;

    /**
     * 返款总价格
     */
    private Double returnTotalPrice;

    /**
     * 实收金额
     */
    private Double actualPrice;

    public ProductInventoryDetailDTO(ProductInventoryDetail productInventoryDetail){
        BeanUtil.copyProperties(productInventoryDetail, this);
    }

    public ProductInventoryDetailDTO toSalesOrder(SalesOrder salesOrder){
        if(salesOrder != null){
            this.price = -salesOrder.getSalePrice();
            this.paymentAmount = -salesOrder.getSaleTotalPrice();
            this.noTaxPaid = -salesOrder.getNoTaxSalePrice();
            this.noTaxAmount = -salesOrder.getNoTaxSaleTotalPrice();
        }
        return this;
    }

    public ProductInventoryDetailDTO toReturnOrder(ReturnOrder returnOrder){

        this.returnTotalPrice = returnOrder.getRefundPrice();
        this.actualPrice = returnOrder.getReturnTotalPrice();
        return this;
    }
}
