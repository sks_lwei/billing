package com.hmkj.crm.billing.web.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.hmkj.crm.billing.domain.company.DeliveryCompaniesInfo;
import com.hmkj.crm.billing.domain.company.SupplierInfo;
import com.hmkj.crm.billing.dto.Message;
import com.hmkj.crm.billing.service.company.DeliveryCompaniesInfoService;
import com.hmkj.crm.billing.service.company.SupplierInfoService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * 公司控制器
 *
 * @author sks.lwei
 * @date 2020/01/19
 */
@RestController
@RequestMapping("/company")
public class CompanyController {

    @Resource
    private SupplierInfoService supplierInfoService;

    @Resource
    private DeliveryCompaniesInfoService deliveryCompaniesInfoService;

    /*--------------------Supplier------------------------------------*/

    @GetMapping("/supplier/select")
    public Message selectSupplierItem(Integer type){
        QueryWrapper<SupplierInfo> wrapper = new QueryWrapper<>();
        if(type != 0){
            wrapper.and(w -> w.eq("type", SupplierInfo.NORMAL).or().eq("type", type));
        }else {
            wrapper.eq("type", SupplierInfo.NORMAL);
        }
        return Message.data(supplierInfoService.selectItem(wrapper));
    }

    @GetMapping("/listSupplier")
    public Message listSupplier(SupplierInfo supplierInfo){
        List<SupplierInfo> supplierInfos = supplierInfoService.listSupplierInfo(supplierInfo);

        return Message.data(supplierInfos);
    }

    @RequestMapping("/supplier/listData")
    public Message listSupplierData(Page<SupplierInfo> page, SupplierInfo supplierInfo){
        IPage<SupplierInfo> supplierInfoPage = supplierInfoService.pageSupplierInfo(page, supplierInfo);
        return Message.page(supplierInfoPage);
    }

    @GetMapping("/supplier/info")
    public Message getInfo(Long id){
        SupplierInfo supplierInfo = supplierInfoService.getSupplierInfo(id);
        return Message.data(supplierInfo);
    }

    @PostMapping({"/supplier/save", "/supplier/edit"})
    public Message saveOrEdit(SupplierInfo supplierInfo){
        if(supplierInfo.getId() == null){
            supplierInfoService.saveSupplierInfo(supplierInfo);
        }else {
            supplierInfoService.editSupplierInfo(supplierInfo.getId(), supplierInfo);
        }
        return Message.SUCCESS;
    }

    @RequestMapping("/supplier/del")
    public Message del(Long id){
        supplierInfoService.removeSupplierInfo(id);
        return Message.SUCCESS;
    }


    /*--------------------DeliveryCompanies------------------------------------*/

    @GetMapping("/delivery/select")
    public Message selectDeliveryItem(){
        return Message.data(deliveryCompaniesInfoService.selectItem(new QueryWrapper<>()));
    }

    @GetMapping("/delivery/list")
    public Message listDelivery(DeliveryCompaniesInfo deliveryCompaniesInfo){
        List<DeliveryCompaniesInfo> deliveryCompaniesInfos = deliveryCompaniesInfoService.listDeliveryCompaniesInfo(deliveryCompaniesInfo);
        return Message.data(deliveryCompaniesInfos);
    }

    @RequestMapping("/delivery/listData")
    public Message listDeliveryData(Page<DeliveryCompaniesInfo> page, DeliveryCompaniesInfo deliveryCompaniesInfo){

        IPage<DeliveryCompaniesInfo> deliveryCompaniesInfoPage = deliveryCompaniesInfoService.pageDeliveryCompaniesInfo(page, deliveryCompaniesInfo);
        return Message.page(deliveryCompaniesInfoPage);
    }

    @GetMapping("/delivery/info")
    public Message getDeliveryInfo(Long id){
        DeliveryCompaniesInfo deliveryCompaniesInfo = deliveryCompaniesInfoService.getDeliveryCompaniesInfo(id);
        return Message.data(deliveryCompaniesInfo);
    }

    @GetMapping("/delivery/name")
    public Message getDeliveryName(String uuid){
        String companiesName = deliveryCompaniesInfoService.getDeliveryCompaniesNameByUuid(uuid);
        return Message.data(companiesName);
    }

    @PostMapping({"/delivery/save", "/delivery/edit"})
    public Message saveOrEditDelivery(DeliveryCompaniesInfo deliveryCompaniesInfo){
        if(deliveryCompaniesInfo.getId() == null){
            deliveryCompaniesInfoService.saveDeliveryCompaniesInfo(deliveryCompaniesInfo);
        }else {
            deliveryCompaniesInfoService.editDeliveryCompaniesInfo(deliveryCompaniesInfo.getId(), deliveryCompaniesInfo);
        }
        return Message.SUCCESS;
    }

    @RequestMapping("/delivery/del")
    public Message delDelivery(Long id){
        deliveryCompaniesInfoService.removeDeliveryCompaniesInfo(id);
        return Message.SUCCESS;
    }
}
