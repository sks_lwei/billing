package com.hmkj.crm.billing.dto;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.annotation.TableField;
import com.hmkj.crm.billing.domain.company.DeliveryCompaniesInfo;
import com.hmkj.crm.billing.domain.order.SalesOrder;
import com.hmkj.crm.billing.domain.product.ProductInfo;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

/**
 * 销售订单DTO
 *
 * @author sks.lwei
 * @date 2020/02/26
 */
@Getter
@Setter
@ToString
@NoArgsConstructor
public class SalesOrderDTO extends SalesOrder implements Serializable {

    /**
     * 产品名称
     */
    private String productName;

    /**
     * 配送公司名称
     */
    private String deliveryName;
    private String deliveryUuid;

    /**
     * 供应商名称
     */
    private String supplierName;
    private String supplierUuid;

    /**
     * 制造商名称
     */
    private String makerName;
    private String makerUuid;

    /**
     * 规格
     */
    private String standard;


    //从进货单获取数据
    /**
     * 含税进价(采购单价)
     */
    private Double taxPaid;

    /**
     * 含税金额(付款额)
     */
    private Double taxAmount;


    public SalesOrderDTO(SalesOrder salesOrder){
        BeanUtil.copyProperties(salesOrder, this);
    }

    public SalesOrderDTO toSalesOrderDTO(ProductInfo productInfo, DeliveryCompaniesInfo deliveryCompaniesInfo){
        this.productName = productInfo.getProductName();
        if(deliveryCompaniesInfo != null){
            this.deliveryName = deliveryCompaniesInfo.getFullName();
            this.deliveryUuid = deliveryCompaniesInfo.getUuid();
        }
        this.supplierName = productInfo.getSupplierName();
        this.supplierUuid = productInfo.getSupplierUuid();
        this.makerName = productInfo.getMakerName();
        this.makerUuid = productInfo.getMakerUuid();
        this.standard = productInfo.getStandard();
        return this;
    }
}
