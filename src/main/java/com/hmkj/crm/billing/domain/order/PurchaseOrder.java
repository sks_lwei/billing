package com.hmkj.crm.billing.domain.order;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.hmkj.crm.billing.domain.BaseDomain;
import com.hmkj.crm.billing.enums.InventoryTypeEnum;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.Date;

/**
 * 采购订单(入库)
 *
 * @author sks.lwei
 * @date 2020/01/16
 */
@Getter
@Setter
@TableName("purchase_order")
@ToString(callSuper = true)
public class PurchaseOrder extends BaseDomain<PurchaseOrder> implements Order{

    /**
     * 采购产品Uuid
     */
    @TableField("product_uuid")
    private String productUuid;

    /**
     * 采购来源公司 uuid
     */
    @TableField("delivery_uuid")
    private String deliveryUuid;

    /**
     * 采购名称
     */
    @TableField("purchase_name")
    private String purchaseName;

    /**
     * 订单时间
     */
    @JsonFormat(pattern="yyyy-MM-dd", timezone = "GMT+8")
    @TableField("order_time")
    private Date orderTime;

    /**
     * 数量
     */
    @TableField
    private Integer number;

    /**
     * 含税进价(采购单价)
     */
    @TableField
    private Double price;

    /**
     * 含税金额(付款额)
     */
    @TableField("payment_amount")
    private Double paymentAmount;

    /**
     * 不含税进价
     */
    @TableField("no_tax_paid")
    private Double noTaxPaid;

    /**
     * 不含税金额
     */
    @TableField("no_tax_amount")
    private Double noTaxAmount;

    /**
     * 是否到达
     */
    @TableField
    private Boolean arrive;

    /**
     * 到货时间
     */
    @TableField("arrive_time")
    private Date arriveTime;

    /**
     * 返款价格
     */
    @TableField("return_price")
    private Double returnPrice;

    /**
     * 返款总价格
     */
    @TableField("return_total_price")
    private Double returnTotalPrice;

    /**
     * 返款实收金额（实返）
     */
    @TableField("actual_price")
    private Double actualPrice;

    /**
     * 返款的人或单位
     */
    @TableField("refund_people")
    private String refundPeople;

    /**
     * 返款时间
     */
    @TableField("return_time")
    private Date returnTime;

    /**
     * 手续费
     */
    @TableField
    private Double poundage;

    /**
     * 备注
     */
    @TableField
    private String remark;

    @Override
    public InventoryTypeEnum getOrderInventoryType() {
        return InventoryTypeEnum.INCOME;
    }

    @Override
    public String getOrderUuid() {
        return this.getUuid();
    }

    @Override
    public Double getTotalPrice() {
        return this.getPaymentAmount();
    }

    /**
     * 转换为销售订单
     *
     * 目前只转换部分字段
     *
     * 对应参照 {@link SalesOrder#toPurchaseOrder()}
     * @return {@link SalesOrder}
     */
    public SalesOrder toSalesOrder(){
        SalesOrder salesOrder = new SalesOrder();
        salesOrder.setUuid(this.getUuid());
        salesOrder.setProductUuid(this.productUuid);
        salesOrder.setNumber(this.number);
        salesOrder.setSaleTotalPrice(this.paymentAmount);
        return salesOrder;
    }
}
