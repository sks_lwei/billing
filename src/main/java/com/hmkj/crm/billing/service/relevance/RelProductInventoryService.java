package com.hmkj.crm.billing.service.relevance;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.hmkj.crm.billing.domain.ProductBill;
import com.hmkj.crm.billing.domain.order.Order;
import com.hmkj.crm.billing.domain.order.PurchaseOrder;
import com.hmkj.crm.billing.domain.relevance.RelProductInventory;
import com.hmkj.crm.billing.dto.ProductInventoryDTO;
import com.hmkj.crm.billing.export.ExportComponent;

import java.util.List;

/**
 * Rel产品库存服务
 *
 * @author sks.lwei
 * @date 2020/01/22
 */
public interface RelProductInventoryService extends IService<RelProductInventory> {


    /**
     * 获取产品库存清单
     *
     * @param page    页面
     * @param wrapper 包装器
     * @return {@link IPage<ProductInventoryDTO>}
     */
    IPage<ProductInventoryDTO> listProductInventoryPage(Page<RelProductInventory> page, Wrapper<RelProductInventory> wrapper);

    /**
     * 增加
     *  @param order  订单
     *  @param deliveryUuid 供货商Uuid
     */
    void up(String deliveryUuid, Order order);

    /**
     * 减少
     *
     * @param order  订单
     * @param deliveryUuid 供货商Uuid
     */
    void down(String deliveryUuid, Order order);

    /**
     * 撤销库存记录
     *
     * @param deliveryUuid 供货商Uuid
     * @param productUuid  产品uuid
     * @param orderUuid 订单号
     */
    void undo(String deliveryUuid, String productUuid, String orderUuid);

    /**
     * 导出
     *
     * @param wrapper 包装器
     */
    List<? extends ExportComponent> export(QueryWrapper<RelProductInventory> wrapper);

}
