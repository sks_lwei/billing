package com.hmkj.crm.billing.query.product;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.bean.copier.CopyOptions;
import cn.hutool.core.text.StrSpliter;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.hmkj.crm.billing.domain.ProductBill;
import com.hmkj.crm.billing.query.QueryComponent;
import com.hmkj.crm.billing.query.annotation.AdvancedQuery;
import com.hmkj.crm.billing.query.enums.QueryMethodEnum;
import com.hmkj.crm.billing.util.StrKit;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.extern.log4j.Log4j2;
import org.apache.poi.ss.formula.functions.T;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

/**
 * 产品查询
 *
 * @author sks.lwei
 * @date 2020/02/27
 */
@Log4j2
@Getter
@Setter
@ToString
public class ProductBillQuery implements QueryComponent<ProductBill> {

    /**
     * 产品名称
     */
    public String productUuid;

    /**
     * 供应商名称
     */
    public String supplierUuid;

    /**
     * 生产企业名称
     */
    public String makerUuid;

    /**
     * 配送公司
     */
    public String deliveryUuid;

    /**
     * 规格
     */
    @AdvancedQuery(method = QueryMethodEnum.LIKE)
    public String standard;

    /**
     * 订单时间
     */
    @AdvancedQuery(method = QueryMethodEnum.BETWEEN)
    public String orderTime;

    /**
     * 入库时间
     */
    @AdvancedQuery(method = QueryMethodEnum.BETWEEN)
    public String arriveTime;

    /**
     * 返款时间
     */
    @AdvancedQuery(method = QueryMethodEnum.BETWEEN)
    public String rebatesTime;


}
