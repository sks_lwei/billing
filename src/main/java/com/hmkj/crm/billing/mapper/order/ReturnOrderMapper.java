package com.hmkj.crm.billing.mapper.order;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hmkj.crm.billing.domain.order.ReturnOrder;

/**
 * 退货映射器
 *
 * @author sks.lwei
 * @date 2020/02/26
 */
public interface ReturnOrderMapper extends BaseMapper<ReturnOrder> {
}
