package com.hmkj.crm.billing.service.company.impl;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hmkj.crm.billing.domain.company.InventoryDetail;
import com.hmkj.crm.billing.domain.order.PurchaseOrder;
import com.hmkj.crm.billing.domain.product.ProductInfo;
import com.hmkj.crm.billing.enums.AmountTypeEnum;
import com.hmkj.crm.billing.export.ExportComponent;
import com.hmkj.crm.billing.export.domain.InventoryDetailExport;
import com.hmkj.crm.billing.mapper.company.InventoryDetailMapper;
import com.hmkj.crm.billing.service.ProductBillService;
import com.hmkj.crm.billing.service.company.DeliveryCompaniesInfoService;
import com.hmkj.crm.billing.service.company.InventoryDetailService;
import com.hmkj.crm.billing.service.order.PurchaseOrderService;
import com.hmkj.crm.billing.service.product.ProductInfoService;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 账单 服务
 *
 * @author sks.lwei
 * @date 2020/02/20
 */
@Log4j2
@Service
public class InventoryDetailServiceImpl extends ServiceImpl<InventoryDetailMapper, InventoryDetail> implements InventoryDetailService {

    /**
     * 配送公司
     */
    @Resource
    private DeliveryCompaniesInfoService deliveryCompaniesInfoService;

    /**
     * 购买订单服务
     */
    @Resource
    private PurchaseOrderService purchaseOrderService;

    /**
     * 产品信息服务
     */
    @Resource
    private ProductInfoService productInfoService;

    @Override
    public IPage<InventoryDetail> listInventoryDetailPage(Page<InventoryDetail> page, Wrapper<InventoryDetail> wrapper) {
        return this.page(page, wrapper);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public synchronized void addDetail(InventoryDetail inventoryDetail) {
        log.info("添加公司账户账单操作");

        inventoryDetail.setUuid("inv_"+ IdWorker.get32UUID());
        //获取公司账户余额
        Double deliveryBalance = deliveryCompaniesInfoService.getDeliveryBalance(inventoryDetail.getCompanyUuid());
        //操作金额
        Double amount = inventoryDetail.getAmount();

        //判定记录方式
        AmountTypeEnum amountType = inventoryDetail.getAmountType();
        switch (amountType){
            //收入
            case INCOME:
            //回款
            case RECEIVABLE:
                deliveryBalance = deliveryBalance + amount;
                break;
            //支出
            case SPENDING:
            //提款
            case WITHDRAWALS:
            //手续费
            case POUNDAGE:
            //配送费
            case SHIPPING_FEE:
                deliveryBalance = deliveryBalance - amount;
                inventoryDetail.setAmount(-amount);
                break;
            default:
                log.warn("未知的金额类型，操作已终止");
                throw new NullPointerException("未知的金额类型");
        }
        inventoryDetail.setBalance(deliveryBalance);
        inventoryDetail.setBillTime(new Date());
        this.save(inventoryDetail);
        log.info("公司账户账单记录添加完成！");

        deliveryCompaniesInfoService.setDeliveryBalance(inventoryDetail.getCompanyUuid(), deliveryBalance);
        log.info("公司账户更新成功");

    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void accountChange(String orderUuid, String companyUuid, Double amount, AmountTypeEnum amountType, String mark) {
        log.info("公司账户记录！类型：{}[{}]，公司：{}，订单号：{}，变动金额：{}", amountType.getTitle(), amountType.getType() > 0 ? "收入" : "支出", companyUuid, orderUuid, amount);

        String companiesName = deliveryCompaniesInfoService.getDeliveryCompaniesNameByUuid(companyUuid);
        if(StrUtil.isBlank(companiesName)){
            throw new NullPointerException("选定公司不存在！");
        }

        InventoryDetail inventoryDetail = new InventoryDetail();
        inventoryDetail.setCompanyUuid(companyUuid);
        inventoryDetail.setCompanyName(companiesName);
        //采购支出
        inventoryDetail.setAmountType(amountType);
        inventoryDetail.setOrderUuid(orderUuid);
        inventoryDetail.setAmount(amount);
        inventoryDetail.setRemark(StrUtil.isBlank(mark) ? "系统自动记录："+amountType.getTitle() : mark);
        this.addDetail(inventoryDetail);
        log.info("系统自动记录账户变更成功");
    }

    @Override
    public void undoAccountChange(String orderUuid, String companyUuid, AmountTypeEnum amountType) {

        QueryWrapper<InventoryDetail> wrapper = new QueryWrapper<>();
        wrapper.eq("order_uuid", orderUuid).eq("company_uuid", companyUuid).eq("amount_type", amountType.getCode());
        InventoryDetail inventoryDetail = this.getOne(wrapper);

        if(inventoryDetail == null){
            log.warn("公司账户[{}],订单记录[{}]不存在，删除失败!,类型:[{}]", companyUuid, orderUuid, amountType.getCode());
            throw new NullPointerException("公司账户记录不存在，删除失败");
        }

        //查询公司账户余额
        Double companyAmount = deliveryCompaniesInfoService.getDeliveryBalance(inventoryDetail.getCompanyUuid());

        Double currentAmount = companyAmount - inventoryDetail.getAmount();
        deliveryCompaniesInfoService.setDeliveryBalance(inventoryDetail.getCompanyUuid(), currentAmount);
        log.info("更新配送公司账户余额,[{}]->[{}]", companyAmount, currentAmount);

        log.info("删除 账户明细， id:{}", inventoryDetail.getId());
        this.removeById(inventoryDetail.getId());
    }

    @Override
    public List<? extends ExportComponent> export(QueryWrapper<InventoryDetail> wrapper) {
        List<InventoryDetail> inventoryDetails = this.list(wrapper);
        return inventoryDetails.stream().map(inventoryDetail -> {
            InventoryDetailExport export = new InventoryDetailExport(inventoryDetail);
            String orderUuid = inventoryDetail.getOrderUuid();
            //根据客户要求，导出此部分信息
            //目前只针对采购订单做导出
            if(StrUtil.isNotBlank(orderUuid) && orderUuid.startsWith("B")){
                PurchaseOrder purchaseOrder = purchaseOrderService.getPurchaseOrderByUuid(orderUuid);
                if(purchaseOrder != null) {
                    ProductInfo productInfo = productInfoService.getProductInfoByUuid(purchaseOrder.getProductUuid());
                    export.buildProductInfo(productInfo, purchaseOrder);
                }
            }
            return export;
        }).collect(Collectors.toList());
    }
}
