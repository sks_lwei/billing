package com.hmkj.crm.billing.query.inventory;

import com.hmkj.crm.billing.domain.company.InventoryDetail;
import com.hmkj.crm.billing.query.QueryComponent;
import com.hmkj.crm.billing.query.annotation.AdvancedQuery;
import com.hmkj.crm.billing.query.enums.QueryMethodEnum;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * 库存详细查询
 *
 * @author sks.lwei
 * @date 2020/02/28
 */
@Getter
@Setter
@ToString
public class InventoryDetailQuery implements QueryComponent<InventoryDetail> {

    /**
     * 订单Uuid
     */
    public String orderUuid;

    /**
     * 公司Uuid
     */
    public String companyUuid;

    /**
     * 金额类型
     */
    public String amountType;

    /**
     * 记账时间
     */
    @AdvancedQuery(method = QueryMethodEnum.BETWEEN)
    public String billTime;
}
