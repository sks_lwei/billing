package com.hmkj.crm.billing.web.controller;


import cn.hutool.core.bean.BeanUtil;
import com.alibaba.druid.support.json.JSONUtils;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.hmkj.crm.billing.domain.ProductBill;
import com.hmkj.crm.billing.domain.order.PurchaseOrder;
import com.hmkj.crm.billing.domain.order.SalesOrder;
import com.hmkj.crm.billing.dto.Message;
import com.hmkj.crm.billing.export.ExcelExportComponent;
import com.hmkj.crm.billing.export.ExportComponent;
import com.hmkj.crm.billing.export.domain.ExcelData;
import com.hmkj.crm.billing.query.AdvancedQueryComponent;
import com.hmkj.crm.billing.query.product.ProductBillQuery;
import com.hmkj.crm.billing.service.ProductBillService;
import com.hmkj.crm.billing.service.order.PurchaseOrderService;
import com.hmkj.crm.billing.util.ExcelUtils;
import org.springframework.web.bind.annotation.*;
import org.thymeleaf.util.ListUtils;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * 产品账单 控制器
 *
 * @author sks.lwei
 * @date 2020/01/16
 */
@RestController
@RequestMapping("/productBill")
public class ProductBillController extends BaseController {

    @Resource
    private ProductBillService productBillService;

    @Resource
    private PurchaseOrderService purchaseOrderService;

    @RequestMapping("/listData")
    public Message list(Page<ProductBill> page, ProductBillQuery productQuery){
        AdvancedQueryComponent<ProductBill> component = new AdvancedQueryComponent<>();
        QueryWrapper<ProductBill> wrapper = component.buildQueryWrapper(productQuery, ProductBill.class);
        IPage<ProductBill> res = productBillService.pageProductBill(page, wrapper);
        return Message.page(res);
    }

    /**
     * 保存(采购入库)
     *
     * @param purchaseOrder 采购订单
     * @param productUuid   产品Uuid
     * @return {@link Message}
     */
    @RequestMapping("/purchase")
    public Message save(String productUuid, PurchaseOrder purchaseOrder){
        productBillService.purchase(productUuid, purchaseOrder);
        return Message.SUCCESS;
    }


    /**
     * 订单产品 到达
     *
     */
    @RequestMapping("/arrive")
    public Message arrive(Long id){
        productBillService.arrive(id);
        return Message.SUCCESS;
    }

    /**
     * 销售(订单出库)
     *
     * @param productUuid  产品Uuid
     * @param salesOrder   销售订单
     * @param deliveryUuid 配送公司Uuid（货源）
     * @return {@link Message}
     */
    @RequestMapping("/sales")
    public Message sales(String productUuid, String deliveryUuid, SalesOrder salesOrder){
        productBillService.sales(productUuid, deliveryUuid, salesOrder);
        return Message.SUCCESS;
    }

    /**
     * 产品订单信息
     *
     * @param id id
     * @return {@link Message}
     */
    @GetMapping("/info")
    public Message info(Long id){
        ProductBill productBillInfo = productBillService.getProductBillInfo(id);
        PurchaseOrder purchaseOrder = purchaseOrderService.getPurchaseOrderByUuid(productBillInfo.getPurchaseOder());
        return Message.data(productBillInfo).put("price", purchaseOrder.getPrice());
    }

    /**
     * 编辑产品账单
     *
     * @param productUuid   产品Uuid
     * @param purchaseOrder 采购订单
     * @return {@link Message}
     */
    @PostMapping("/edit")
    public Message editBill(String productUuid, PurchaseOrder purchaseOrder){

        System.out.println(productUuid);
        System.out.println(purchaseOrder);
        return Message.FAIL;
    }

    /**
     * 撤回
     */
    @GetMapping("/undo")
    public Message toWithdraw(Long id, String mark){
        productBillService.unPurchase(id, mark);
        return Message.SUCCESS;
    }

    /**
     * 得到手续费信息
     *
     * @param orderNumber 订单号
     * @return {@link Message}
     */
    @GetMapping("/poundageInfo")
    public Message getPoundageInfo(@RequestParam("ouuid") String orderNumber){
        Map<String, Object> poundageInfo = purchaseOrderService.getPoundageInfo(orderNumber);
        return Message.data(poundageInfo);
    }

    @PostMapping("/export")
    public void export(HttpServletRequest request, HttpServletResponse response, ProductBillQuery productQuery){
        Message message = null;
        String header = request.getHeader("export-type");
        QueryWrapper<ProductBill> wrapper = null;

        switch (header){
            case ExcelUtils.EXPORT_ALL:
                System.out.println("EXPORT_ALL");
                AdvancedQueryComponent<ProductBill> component = new AdvancedQueryComponent<>();
                wrapper = component.buildQueryWrapper(productQuery, ProductBill.class);
                break;
            case ExcelUtils.EXPORT_SELECT:
                String[] ids = request.getParameterValues("ids");
                System.out.println("EXPORT_SELECT");
                wrapper = new QueryWrapper<>();
                wrapper.in("id", (Object[]) ids);
                break;
            default:
        }

        if(wrapper != null){
            List<ExcelData> excelDataList = new LinkedList<>();
            List<? extends ExportComponent> exportDatas = productBillService.export(wrapper);

            ExcelExportComponent exportComponent = ExcelExportComponent.build();
            ExcelData excelData = exportComponent.creatExcelData(exportDatas);

            excelDataList.add(excelData);
            try {
                ExcelUtils.exportExcel(response, "入库信息列表" + IdWorker.getMillisecond(), excelDataList);
                message = Message.SUCCESS;
            } catch (Exception e) {
                e.printStackTrace();
                message = Message.FAIL;
            }
        }else {
            message = Message.FAIL;
        }

        if(!message.isSuccess()){
            String jsonString = JSONUtils.toJSONString(message);
            try {
                response.getWriter().write(jsonString);
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }

    }

}
