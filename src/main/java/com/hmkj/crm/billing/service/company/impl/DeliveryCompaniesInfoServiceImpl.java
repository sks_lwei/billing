package com.hmkj.crm.billing.service.company.impl;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hmkj.crm.billing.domain.company.DeliveryCompaniesInfo;
import com.hmkj.crm.billing.dto.SelectItemVO;
import com.hmkj.crm.billing.mapper.company.DeliveryCompaniesInfoMapper;
import com.hmkj.crm.billing.service.company.DeliveryCompaniesInfoService;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 配送公司信息 服务Impl
 *
 * 此处高级查询采用简单模式处理
 *
 * @author sks.lwei
 * @date 2020/01/16
 */
@Log4j2
@Service
public class DeliveryCompaniesInfoServiceImpl extends ServiceImpl<DeliveryCompaniesInfoMapper, DeliveryCompaniesInfo> implements DeliveryCompaniesInfoService {

    /**
     * 简单模式查询
     * 条件过滤
     *
     * @param deliveryCompaniesInfo 公司信息
     * @param wrapper               包装器
     */
    private void conditionFilter(DeliveryCompaniesInfo deliveryCompaniesInfo, QueryWrapper<DeliveryCompaniesInfo> wrapper){
        String fullName = deliveryCompaniesInfo.getFullName();
        if(StrUtil.isNotBlank(fullName)){
            wrapper.like("full_name", fullName);
        }
    }

    @Override
    public IPage<DeliveryCompaniesInfo> pageDeliveryCompaniesInfo(Page<DeliveryCompaniesInfo> page, DeliveryCompaniesInfo deliveryCompaniesInfo) {
        QueryWrapper<DeliveryCompaniesInfo> wrapper = new QueryWrapper<>();
        conditionFilter(deliveryCompaniesInfo, wrapper);
        return this.page(page, wrapper);
    }

    @Override
    public List<DeliveryCompaniesInfo> listDeliveryCompaniesInfo(DeliveryCompaniesInfo deliveryCompaniesInfo) {
        QueryWrapper<DeliveryCompaniesInfo> wrapper = new QueryWrapper<>();
        conditionFilter(deliveryCompaniesInfo, wrapper);
        return this.list(wrapper);
    }

    @Override
    public DeliveryCompaniesInfo getDeliveryCompaniesInfo(Long id) {
        return this.getById(id);
    }

    @Override
    public DeliveryCompaniesInfo getDeliveryCompaniesInfoByuuid(String uuid) {
        QueryWrapper<DeliveryCompaniesInfo> wrapper = new QueryWrapper<>();
        wrapper.eq("uuid", uuid);
        return getOne(wrapper);
    }

    @Override
    public String getDeliveryCompaniesNameByUuid(String uuid) {
        QueryWrapper<DeliveryCompaniesInfo> wrapper = new QueryWrapper<>();
        wrapper.select("full_name").eq("uuid", uuid);
        DeliveryCompaniesInfo one = getOne(wrapper);
        if(one!= null){
            return one.getFullName();
        }else {
            return "";
        }
    }

    @Override
    public void saveDeliveryCompaniesInfo(DeliveryCompaniesInfo deliveryCompaniesInfo) {
        log.info("新增配送公司信息，[{}]", deliveryCompaniesInfo);

        deliveryCompaniesInfo.setUuid("deli_" + IdWorker.get32UUID());
        this.save(deliveryCompaniesInfo);
    }

    @Override
    public void editDeliveryCompaniesInfo(Long id, DeliveryCompaniesInfo deliveryCompaniesInfo) {
        log.info("修改配送公司信息，[{}]", deliveryCompaniesInfo);

        deliveryCompaniesInfo.setId(id);
        this.updateById(deliveryCompaniesInfo);
    }

    @Override
    public void removeDeliveryCompaniesInfo(Long id) {
        log.warn("删除配送公司信息，id=[{}]", id);
        this.removeById(id);
    }

    @Override
    public Double getDeliveryBalance(String uuid) {
        QueryWrapper<DeliveryCompaniesInfo> wrapper = new QueryWrapper<>();
        wrapper.select("balance").eq("uuid", uuid);
        return this.getOne(wrapper).getBalance();
    }

    @Override
    public void setDeliveryBalance(String uuid, Double balance) {
        log.warn("更新公司[{}]账户余额[{}]", uuid, balance);
        UpdateWrapper<DeliveryCompaniesInfo> wrapper = new UpdateWrapper<>();
        wrapper.set("balance", balance).eq("uuid", uuid);
        this.update(wrapper);
        log.info("更新公司账户余额成功!");
    }

    @Override
    public List<SelectItemVO> selectItem(QueryWrapper<DeliveryCompaniesInfo> wrapper) {
        wrapper.select("uuid", "full_name");
        List<DeliveryCompaniesInfo> deliveryCompaniesInfos = this.list(wrapper);
        return SelectItemVO.buildList(deliveryCompaniesInfos);
    }
}
