package com.hmkj.crm.billing.dto;

import com.baomidou.mybatisplus.annotation.TableField;
import com.hmkj.crm.billing.domain.product.ProductInfo;
import com.hmkj.crm.billing.export.ExportTarget;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

/**
 * 产品库存 DTO
 *
 * @author sks.lwei
 * @date 2020/02/15
 */
@Getter
@Setter
@ToString
public class ProductInventoryDTO implements Serializable {

    private String uuid;

    /**
     * 采购来源公司
     */
    private String deliveryUuid;
    private String deliveryName;

    /**
     * 库存数量
     */
    private Integer number;

    //采购信息

    /**
     * 含税进价(采购单价)
     */
    private Double taxPaid;

    /**
     * 含税总价格
     */
    private Double taxTotalPrice;

    /**
     * 不含税进价
     */
    private Double noTaxPaid;

    //产品信息

    /**
     * 产品
     */
    private String productUuid;
    private String productName;

    /**
     * 供应商名称
     */
    private String supplierName;

    /**
     * 制造商名称
     */
    private String makerName;

    /**
     * 规格
     */
    private String standard;

    /**
     * 单位
     */
    private String unit;

    /**
     * 件装单位
     */
    private Integer units;

    /**
     * 建立产品信息
     *
     */
    public void buildProductInfo(ProductInfo productInfo){
        if(productInfo != null){
            this.productName = productInfo.getProductName();
            this.supplierName = productInfo.getSupplierName();
            this.makerName = productInfo.getMakerName();
            this.standard = productInfo.getStandard();

            this.unit = productInfo.getUnit();
            this.units = productInfo.getUnits();
        }else {
            this.productName = "";
            this.supplierName = "";
            this.makerName = "";
            this.standard = "";

            this.unit = "";
            this.units = 0;
        }

    }
}
