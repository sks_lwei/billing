package com.hmkj.crm.billing.export.domain;

import cn.hutool.core.bean.BeanUtil;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.hmkj.crm.billing.domain.order.SalesOrder;
import com.hmkj.crm.billing.export.ExportTarget;

import java.util.Date;

/**
 * 销售订单的 导出
 *
 * @author sks.lwei
 * @date 2020/03/03
 */
public class SalesOrderExport extends ProductExport {

    /**
     * 配送公司名称
     */
    @ExportTarget(title = "配送公司")
    public String deliveryName;

    /**
     * 销售时间(出库时间)
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm", timezone = "GMT+8")
    @ExportTarget(title = "销售时间")
    public Date salesTime;

    /**
     * 数量
     */
    @ExportTarget(title = "数量")
    public Integer number;

    /**
     * 含税销价(销售价格)
     */
    @ExportTarget(title = "含税销价")
    public Double salePrice;

    /**
     * 含税销价金额(销售总价格)
     */
    @ExportTarget(title = "含税销售总金额")
    public Double saleTotalPrice;

    /**
     * 不含税销价
     */
    @ExportTarget(title = "不含税销价")
    public Double noTaxSalePrice;

    /**
     * 不含税销价金额
     */
    @ExportTarget(title = "不含销售总金额")
    public Double noTaxSaleTotalPrice;

    /**
     * 含税进价(采购单价)
     */
    @ExportTarget(title = "含税进价")
    public Double taxPaid;

    /**
     * 含税进价金额(付款额)
     */
    @ExportTarget(title = "含税购进总金额")
    public Double taxAmount;

    /**
     * 客户名称
     */
    @ExportTarget(title = "客户名称")
    public String customerName;

    /**
     * 备注
     */
    @ExportTarget(title = "备注")
    public String remark;

    public SalesOrderExport(SalesOrder salesOrders) {
        BeanUtil.copyProperties(salesOrders, this);
    }
}
