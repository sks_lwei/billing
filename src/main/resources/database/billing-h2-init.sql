SET MODE MYSQL;

-- ----------------------------
-- Table structure for delivery_companies_info
-- ----------------------------
DROP TABLE IF EXISTS delivery_companies_info;
CREATE TABLE delivery_companies_info (
                                         id int(50) NOT NULL AUTO_INCREMENT ,
                                         uuid varchar(50) DEFAULT NULL ,
                                         full_name varchar(100) DEFAULT NULL ,
                                         balance double(10) DEFAULT '0.00' ,
                                         remark text ,
                                         sort int(10) DEFAULT NULL ,
                                         yn tinyint(1) DEFAULT '0' ,
                                         update_time datetime DEFAULT NULL ,
                                         create_time datetime DEFAULT NULL ,
                                         PRIMARY KEY (id)
);

-- ----------------------------
-- Table structure for inventory_detail
-- ----------------------------
DROP TABLE IF EXISTS inventory_detail;
CREATE TABLE inventory_detail (
                                  id int(50) NOT NULL AUTO_INCREMENT ,
                                  uuid varchar(50) DEFAULT NULL ,
                                  order_uuid varchar(50) DEFAULT NULL ,
                                  company_uuid varchar(50) DEFAULT NULL ,
                                  company_name varchar(50) DEFAULT NULL ,
                                  amount_type varchar(10) DEFAULT NULL ,
                                  amount double(10) DEFAULT '0.00' ,
                                  balance double(10) DEFAULT NULL ,
                                  bill_time datetime DEFAULT NULL ,
                                  remark text ,
                                  sort int(10) DEFAULT NULL ,
                                  yn tinyint(1) DEFAULT '0' ,
                                  update_time datetime DEFAULT NULL ,
                                  create_time datetime DEFAULT NULL ,
                                  PRIMARY KEY (id)
);

-- ----------------------------
-- Table structure for product_bill
-- ----------------------------
DROP TABLE IF EXISTS product_bill;
CREATE TABLE product_bill (
                              id int(50) NOT NULL AUTO_INCREMENT ,
                              uuid varchar(50) DEFAULT NULL ,
                              purchase_order varchar(50) DEFAULT NULL ,
                              purchase_name varchar(100) DEFAULT NULL ,
                              product_name varchar(100) DEFAULT NULL ,
                              product_uuid varchar(50) DEFAULT NULL ,
                              supplier_name varchar(100) DEFAULT NULL ,
                              supplier_uuid varchar(50) DEFAULT NULL ,
                              maker_name varchar(100) DEFAULT NULL ,
                              maker_uuid varchar(50) DEFAULT NULL ,
                              delivery_name varchar(100) DEFAULT NULL ,
                              delivery_uuid varchar(50) DEFAULT NULL ,
                              standard varchar(20) DEFAULT NULL ,
                              unit varchar(20) DEFAULT NULL ,
                              units int(10) DEFAULT NULL ,
                              sale_price double(10) DEFAULT NULL ,
                              cost_price double(10) DEFAULT NULL ,
                              order_time datetime DEFAULT NULL ,
                              number int(11) DEFAULT NULL ,
                              payment_amount double(10) DEFAULT NULL ,
                              poundage double(10) DEFAULT NULL ,
                              arrive tinyint(1) DEFAULT '0' ,
                              arrive_time datetime DEFAULT NULL ,
                              rebates tinyint(1) DEFAULT '0' ,
                              rebates_time datetime DEFAULT NULL ,
                              remark text ,
                              sort int(10) DEFAULT NULL ,
                              yn tinyint(1) DEFAULT '0' ,
                              update_time datetime DEFAULT NULL ,
                              create_time datetime DEFAULT NULL ,
                              PRIMARY KEY (id)
);

-- ----------------------------
-- Table structure for product_info
-- ----------------------------
DROP TABLE IF EXISTS product_info;
CREATE TABLE product_info (
                              id int(50) NOT NULL AUTO_INCREMENT ,
                              uuid varchar(50) DEFAULT NULL ,
                              product_name varchar(100) DEFAULT NULL ,
                              supplier_name varchar(100) DEFAULT NULL ,
                              supplier_uuid varchar(50) DEFAULT NULL,
                              maker_name varchar(100) DEFAULT NULL ,
                              maker_uuid varchar(50) DEFAULT NULL,
                              standard varchar(20) DEFAULT NULL ,
                              unit varchar(20) DEFAULT NULL ,
                              units int(10) DEFAULT NULL ,
                              rate double(10) DEFAULT '0.00' ,
                              sale_price double(10) DEFAULT NULL ,
                              cost_price double(10) DEFAULT NULL ,
                              return_price double(10) DEFAULT NULL ,
                              remark text ,
                              sort int(10) DEFAULT NULL ,
                              yn tinyint(1) DEFAULT '0' ,
                              update_time datetime DEFAULT NULL ,
                              create_time datetime DEFAULT NULL ,
                              PRIMARY KEY (id)
);

-- ----------------------------
-- Table structure for product_inventory_detail
-- ----------------------------
DROP TABLE IF EXISTS product_inventory_detail;
CREATE TABLE product_inventory_detail (
                                          id int(50) NOT NULL AUTO_INCREMENT ,
                                          uuid varchar(50) DEFAULT NULL ,
                                          order_uuid varchar(50) DEFAULT NULL ,
                                          inventory_uuid varchar(50) DEFAULT NULL ,
                                          inventory_type varchar(10) DEFAULT NULL ,
                                          number int(10) DEFAULT '0' ,
                                          total_price double(10) DEFAULT NULL ,
                                          sort int(10) DEFAULT NULL ,
                                          yn tinyint(1) DEFAULT '0' ,
                                          update_time datetime DEFAULT NULL ,
                                          create_time datetime DEFAULT NULL ,
                                          PRIMARY KEY (id)
);

-- ----------------------------
-- Table structure for purchase_order
-- ----------------------------
DROP TABLE IF EXISTS purchase_order;
CREATE TABLE purchase_order (
                                id int(50) NOT NULL AUTO_INCREMENT ,
                                uuid varchar(50) DEFAULT NULL ,
                                product_uuid varchar(50) DEFAULT NULL ,
                                delivery_uuid varchar(50) DEFAULT NULL ,
                                purchase_name varchar(100) DEFAULT NULL ,
                                order_time datetime DEFAULT NULL ,
                                number int(11) DEFAULT NULL ,
                                price double(10) DEFAULT NULL ,
                                payment_amount double(10) DEFAULT NULL ,
                                no_tax_paid double(10) DEFAULT NULL ,
                                no_tax_amount double(10) DEFAULT NULL ,
                                arrive tinyint(1) DEFAULT NULL ,
                                arrive_time datetime DEFAULT NULL ,
                                return_price double(10) DEFAULT NULL ,
                                return_total_price double(10) DEFAULT NULL ,
                                actual_price double(10) DEFAULT NULL ,
                                refund_people varchar(100) DEFAULT NULL ,
                                return_time datetime DEFAULT NULL ,
                                poundage double(10) DEFAULT '0.00' ,
                                remark text ,
                                sort int(10) DEFAULT NULL ,
                                yn tinyint(1) DEFAULT '0' ,
                                update_time datetime DEFAULT NULL ,
                                create_time datetime DEFAULT NULL ,
                                PRIMARY KEY (id)
);

-- ----------------------------
-- Table structure for rel_product_inventory
-- ----------------------------
DROP TABLE IF EXISTS rel_product_inventory;
CREATE TABLE rel_product_inventory (
                                       id int(50) NOT NULL AUTO_INCREMENT ,
                                       uuid varchar(50) DEFAULT NULL ,
                                       product_uuid varchar(50) DEFAULT NULL ,
                                       delivery_uuid varchar(50) DEFAULT NULL ,
                                       number int(10) DEFAULT '0' ,
                                       sort int(10) DEFAULT NULL ,
                                       yn tinyint(1) DEFAULT '0' ,
                                       update_time datetime DEFAULT NULL ,
                                       create_time datetime DEFAULT NULL ,
                                       PRIMARY KEY (id)
);

-- ----------------------------
-- Table structure for return_order
-- ----------------------------
DROP TABLE IF EXISTS return_order;
CREATE TABLE return_order (
                              id int(50) NOT NULL AUTO_INCREMENT ,
                              uuid varchar(50) DEFAULT NULL ,
                              product_uuid varchar(50) DEFAULT NULL ,
                              return_time datetime DEFAULT NULL ,
                              number int(11) DEFAULT '0' ,
                              return_price double(10) DEFAULT '0.00' ,
                              refund_price double(10) DEFAULT '0.00' ,
                              return_total_price double(10) DEFAULT '0.00' ,
                              remark text ,
                              sort int(10) DEFAULT NULL ,
                              yn tinyint(1) DEFAULT '0' ,
                              update_time datetime DEFAULT NULL ,
                              create_time datetime DEFAULT NULL ,
                              PRIMARY KEY (id)
);

-- ----------------------------
-- Table structure for sales_order
-- ----------------------------
DROP TABLE IF EXISTS sales_order;
CREATE TABLE sales_order (
                             id int(50) NOT NULL AUTO_INCREMENT ,
                             uuid varchar(50) DEFAULT NULL ,
                             product_uuid varchar(50) DEFAULT NULL ,
                             delivery_uuid varchar(50) DEFAULT NULL ,
                             sales_time datetime DEFAULT NULL ,
                             number int(11) DEFAULT NULL ,
                             sale_price double(10) DEFAULT NULL ,
                             sale_total_price double(10) DEFAULT NULL ,
                             no_tax_sale_price double(10) DEFAULT NULL ,
                             no_tax_sale_total_price double(10) DEFAULT NULL ,
                             customer_name varchar(100) DEFAULT NULL ,
                             remark text ,
                             sort int(10) DEFAULT NULL ,
                             yn tinyint(1) DEFAULT '0' ,
                             update_time datetime DEFAULT NULL ,
                             create_time datetime DEFAULT NULL ,
                             PRIMARY KEY (id)
);

-- ----------------------------
-- Table structure for supplier_info
-- ----------------------------
DROP TABLE IF EXISTS supplier_info;
CREATE TABLE supplier_info (
                               id int(50) NOT NULL AUTO_INCREMENT ,
                               uuid varchar(50) DEFAULT NULL ,
                               type int(5) DEFAULT '0' ,
                               full_name varchar(100) DEFAULT NULL ,
                               remark text ,
                               sort int(10) DEFAULT NULL ,
                               yn tinyint(1) DEFAULT '0' ,
                               update_time datetime DEFAULT NULL ,
                               create_time datetime DEFAULT NULL ,
                               PRIMARY KEY (id)
);
