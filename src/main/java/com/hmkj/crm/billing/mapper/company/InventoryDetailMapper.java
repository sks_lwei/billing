package com.hmkj.crm.billing.mapper.company;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hmkj.crm.billing.domain.company.InventoryDetail;


/**
 * （账户明细） 映射器
 *
 * @author sks.lwei
 * @date 2020/02/20
 */
public interface InventoryDetailMapper extends BaseMapper<InventoryDetail> {
}
