package com.hmkj.crm.billing.export.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.hmkj.crm.billing.export.ExportComponent;
import com.hmkj.crm.billing.export.ExportTarget;

/**
 * 产品 导出
 *
 * @author sks.lwei
 * @date 2020/03/03
 */
public class ProductExport implements ExportComponent {

    /**
     * 产品名称
     */
    @ExportTarget(title = "产品名称", sort = 99)
    public String productName;

    /**
     * 供应商名称
     */
    @ExportTarget(title = "供应商名称", sort = 98)
    public String supplierName;

    /**
     * 生产企业名称
     */
    @ExportTarget(title = "生产企业名称", sort = 97)
    public String makerName;

    /**
     * 规格
     */
    @ExportTarget(title = "规格", sort = 96)
    public String standard;

    /**
     * 单位
     */
    @ExportTarget(title = "单位", sort = 95)
    public String unit;

    /**
     * 件装单位
     */
    @ExportTarget(title = "件装单位", sort = 94)
    public Integer units;
}
