package com.hmkj.crm.billing.query.enums;

/**
 * 查询方法枚举
 *
 * @author sks.lwei
 * @date 2020/02/27
 */
public enum  QueryMethodEnum {
    /**
     * 特殊查询
     */
    LIKE, BETWEEN;
}
