package com.hmkj.crm.billing.query.product;

import com.hmkj.crm.billing.domain.product.ProductInfo;
import com.hmkj.crm.billing.query.QueryComponent;
import com.hmkj.crm.billing.query.annotation.AdvancedQuery;
import com.hmkj.crm.billing.query.enums.QueryMethodEnum;
import lombok.Getter;
import lombok.Setter;

/**
 * 产品信息查询
 *
 * @author sks.lwei
 * @date 2020/02/28
 */
@Getter
@Setter
public class ProductInfoQuery implements QueryComponent<ProductInfo> {

    @AdvancedQuery(method = QueryMethodEnum.LIKE)
    public String productName;

    /**
     * 供应商Uuid
     */
    public String supplierUuid;

    /**
     * 制造商Uuid
     */
    public String makerUuid;

    /**
     * 标准
     */
    @AdvancedQuery(method = QueryMethodEnum.LIKE)
    public String standard;

    /**
     * 率
     */
    @AdvancedQuery(method = QueryMethodEnum.LIKE)
    public String rate;
}
