package com.hmkj.crm.billing;

import com.hmkj.crm.billing.domain.ProductBill;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

public class GenericDAO<T> {

    public Class<T> entityClass;
    protected GenericDAO() {
        Type type = getClass().getGenericSuperclass();
        Type trueType = ((ParameterizedType) type).getActualTypeArguments()[0];
        this.entityClass = (Class<T>) trueType;
    }

    public static void main(String[] args) throws Exception {
        GenericDAO<ProductBill> manager = new GenericDAO<ProductBill>();
        System.out.println(manager.entityClass);
    }
}



