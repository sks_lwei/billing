package com.hmkj.crm.billing.mapper.relevance;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hmkj.crm.billing.domain.relevance.ProductInventoryDetail;

/**
 * 产品库存详细映射器
 *
 * @author sks.lwei
 * @date 2020/02/15
 */
public interface ProductInventoryDetailMapper extends BaseMapper<ProductInventoryDetail> {
}
