package com.hmkj.crm.billing.export.domain;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.hmkj.crm.billing.domain.company.InventoryDetail;
import com.hmkj.crm.billing.domain.order.PurchaseOrder;
import com.hmkj.crm.billing.domain.product.ProductInfo;
import com.hmkj.crm.billing.enums.AmountTypeEnum;
import com.hmkj.crm.billing.export.ExportComponent;
import com.hmkj.crm.billing.export.ExportTarget;

import java.util.Date;

/**
 * 公司拟账户 详细
 *
 * @author sks.lwei
 * @date 2020/03/03
 */
public class InventoryDetailExport implements ExportComponent {

    /**
     * 订单号
     */
    public String orderUuid;

    /**
     * 配送公司名称
     */
    @ExportTarget(title = "配送公司", sort = 10)
    public String companyName;

    /**
     * 金额类型（spending支出/income收入/receivable回款/poundage手续费/shipping配送费）
     */
    @ExportTarget(title = "金额类型", sort = 10)
    public String amountType;

    /**
     * 金额(根据amountType标识+/-)
     */
    @ExportTarget(title = "金额", sort = 10)
    public Double amount;

    /**
     * 实时余额
     */
    @ExportTarget(title = "实时余额", sort = 10)
    public Double balance;

    /**
     * 记账时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @ExportTarget(title = "记账时间", sort = 10)
    public Date billTime;

    /**
     * 备注
     */
    @ExportTarget(title = "备注", sort = 10)
    public String remark;


    //产品信息

    /**
     * 产品名称
     */
    @ExportTarget(title = "产品名称", sort = 1)
    public String productName;

    /**
     * 供应商名称
     */
    @ExportTarget(title = "供应商名称", sort = 1)
    public String supplierName;

    /**
     * 生产企业名称
     */
    @ExportTarget(title = "生产企业名称", sort = 1)
    public String makerName;

    /**
     * 规格
     */
    @ExportTarget(title = "规格", sort = 1)
    public String standard;

    /**
     * 数量
     */
    @ExportTarget(title = "数量", sort = 1)
    public Integer number;

    /**
     * 成本价格(含税进价)
     */
    @ExportTarget(title = "含税单价", sort = 1)
    public Double costPrice;

    /**
     * 含税总金额
     */
    @ExportTarget(title = "含税总金额", sort = 1)
    public Double paymentAmount;

    public InventoryDetailExport(InventoryDetail inventoryDetail){
        BeanUtil.copyProperties(inventoryDetail, this);
        this.amountType = inventoryDetail.getAmountType().getTitle();
    }

    /**
     * 建立产品信息
     * 根据客户要求 需要导出此部分信息
     *
     * @param productInfo   产品信息
     * @param purchaseOrder 采购订单
     */
    public void buildProductInfo(ProductInfo productInfo, PurchaseOrder purchaseOrder){
        this.productName = productInfo.getProductName();
        this.supplierName = productInfo.getSupplierName();
        this.makerName = productInfo.getMakerName();
        this.standard = productInfo.getStandard();
        this.number = purchaseOrder.getNumber();
        this.costPrice = purchaseOrder.getPrice();
        this.paymentAmount = purchaseOrder.getPaymentAmount();
    };

}
