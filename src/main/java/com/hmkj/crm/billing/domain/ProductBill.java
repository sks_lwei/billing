package com.hmkj.crm.billing.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.hmkj.crm.billing.domain.company.DeliveryCompaniesInfo;
import com.hmkj.crm.billing.domain.order.PurchaseOrder;
import com.hmkj.crm.billing.domain.product.ProductInfo;
import com.hmkj.crm.billing.export.domain.ExcelData;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * 产品账单
 *
 * 用于总的展示 逐条展示
 *
 * @author sks.lwei
 * @date 2020/01/16
 */
@Getter
@Setter
@TableName("product_bill")
public class ProductBill extends BaseDomain<ProductBill> {

    /**
     * 订单号
     */
    @TableField("purchase_order")
    private String purchaseOder;

    /**
     * 采购名称
     */
    @TableField("purchase_name")
    private String purchaseName;

    /**
     * 产品名称
     */
    @TableField("product_name")
    private String productName;
    @TableField("product_uuid")
    private String productUuid;

    /**
     * 供应商名称
     */
    @TableField("supplier_name")
    private String supplierName;
    @TableField("supplier_uuid")
    private String supplierUuid;

    /**
     * 生产企业名称
     */
    @TableField("maker_name")
    private String makerName;
    @TableField("maker_uuid")
    private String makerUuid;

    /**
     * 配送公司
     */
    @TableField("delivery_name")
    private String deliveryName;
    @TableField("delivery_uuid")
    private String deliveryUuid;

    /**
     * 规格
     */
    @TableField
    private String standard;

    /**
     * 单位
     */
    @TableField
    private String unit;

    /**
     * 件装单位
     */
    @TableField
    private Integer units;

    /**
     * 销售价格
     */
    @TableField("sale_price")
    private Double salePrice;

    /**
     * 成本价格(含税进价)
     */
    @TableField("cost_price")
    private Double costPrice;

    /**
     * 订单时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm", timezone = "GMT+8")
    @TableField("order_time")
    private Date orderTime;

    /**
     * 数量
     */
    @TableField
    private Integer number;

    /**
     * 含税总金额
     */
    @TableField("payment_amount")
    private Double paymentAmount;

    /**
     * 手续费
     */
    @TableField
    private Double poundage;

    /**
     * 是否到达
     */
    @TableField
    private Boolean arrive;

    /**
     * 入库时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm", timezone = "GMT+8")
    @TableField("arrive_time")
    private Date arriveTime;

    /**
     * 是否返款
     */
    @TableField
    private Boolean rebates;

    /**
     * 返款时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm", timezone = "GMT+8")
    @TableField("rebates_time")
    private Date rebatesTime;

    /**
     * 备注
     */
    private String remark;

    public ProductBill() {
    }

    public ProductBill(PurchaseOrder purchaseOrder, ProductInfo productInfo, DeliveryCompaniesInfo deliveryCompaniesInfo){

        this.purchaseName = purchaseOrder.getPurchaseName();

        this.productName = productInfo.getProductName();
        this.productUuid = productInfo.getUuid();

        this.supplierName = productInfo.getSupplierName();
        this.supplierUuid = productInfo.getSupplierUuid();

        this.makerName = productInfo.getMakerName();
        this.makerUuid = productInfo.getMakerUuid();

        this.deliveryName = deliveryCompaniesInfo.getFullName();
        this.deliveryUuid = deliveryCompaniesInfo.getUuid();

        this.standard = productInfo.getStandard();
        this.unit = productInfo.getUnit();
        this.units = productInfo.getUnits();

        this.salePrice = productInfo.getSalePrice();
        this.costPrice = productInfo.getCostPrice();

        this.orderTime = purchaseOrder.getOrderTime();
        this.number = purchaseOrder.getNumber();
        this.paymentAmount = purchaseOrder.getPaymentAmount();
        this.arrive = purchaseOrder.getArrive();
        if(this.arrive != null && this.arrive){
            this.arriveTime = new Date();
            if(purchaseOrder.getArriveTime() != null){
                this.arriveTime = purchaseOrder.getArriveTime();
            }
        }
        this.poundage = purchaseOrder.getPoundage();

    }

}
