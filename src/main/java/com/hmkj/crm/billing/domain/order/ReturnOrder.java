package com.hmkj.crm.billing.domain.order;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.hmkj.crm.billing.domain.BaseDomain;
import com.hmkj.crm.billing.enums.InventoryTypeEnum;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

/**
 * 退款订单
 *
 * @author sks.lwei
 * @date 2020/02/26
 */
@Getter
@Setter
@TableName("return_order")
public class ReturnOrder extends BaseDomain<ReturnOrder> implements Order {


    /**
     * 产品Uuid
     */
    @TableField("product_uuid")
    private String productUuid;

    /**
     * 退款时间
     */
    @TableField("return_time")
    private Date returnTime;

    /**
     * 数量
     */
    @TableField
    private Integer number;

    /**
     * 应退款总金额
     */
    @TableField("return_price")
    private Double returnPrice;

    /**
     * 返款退还金额
     */
    @TableField("refund_price")
    private Double refundPrice;

    /**
     * 实际退还总金额
     */
    @TableField("return_total_price")
    private Double returnTotalPrice;

    /**
     * 备注
     */
    @TableField
    private String remark;


    @Override
    public InventoryTypeEnum getOrderInventoryType() {
        return InventoryTypeEnum.RETURN;
    }

    @Override
    public String getOrderUuid() {
        return getUuid();
    }

    @Override
    public Double getTotalPrice() {
        return getReturnTotalPrice();
    }
}
