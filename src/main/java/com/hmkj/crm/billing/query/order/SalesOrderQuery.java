package com.hmkj.crm.billing.query.order;

import com.hmkj.crm.billing.domain.order.SalesOrder;
import com.hmkj.crm.billing.query.QueryComponent;
import com.hmkj.crm.billing.query.annotation.AdvancedQuery;
import com.hmkj.crm.billing.query.enums.QueryMethodEnum;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * 销售订单查询
 *
 * @author sks.lwei
 * @date 2020/02/28
 */
@Getter
@Setter
@ToString
public class SalesOrderQuery implements QueryComponent<SalesOrder> {

    public String productUuid;

//    public String supplierUuid;
//
//    public String makerUuid;

    public String deliveryUuid;

    @AdvancedQuery(method = QueryMethodEnum.LIKE)
    public String customerName;

    @AdvancedQuery(method = QueryMethodEnum.BETWEEN)
    public String salesTime;

}
