package com.hmkj.crm.billing.mapper.company;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hmkj.crm.billing.domain.company.SupplierInfo;

/**
 * 供应商及企业信息Mapper
 *
 * @author sks.li
 * @date 2020/01/16
 */
public interface SupplierInfoMapper extends BaseMapper<SupplierInfo> {
}
