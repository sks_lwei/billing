package com.hmkj.crm.billing.web.controller;

import cn.hutool.core.util.StrUtil;
import com.alibaba.druid.support.json.JSONUtils;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.hmkj.crm.billing.domain.ProductBill;
import com.hmkj.crm.billing.domain.relevance.ProductInventoryDetail;
import com.hmkj.crm.billing.domain.relevance.RelProductInventory;
import com.hmkj.crm.billing.dto.Message;
import com.hmkj.crm.billing.dto.ProductInventoryDTO;
import com.hmkj.crm.billing.dto.ProductInventoryDetailDTO;
import com.hmkj.crm.billing.enums.InventoryTypeEnum;
import com.hmkj.crm.billing.export.ExcelExportComponent;
import com.hmkj.crm.billing.export.ExportComponent;
import com.hmkj.crm.billing.export.domain.ExcelData;
import com.hmkj.crm.billing.query.AdvancedQueryComponent;
import com.hmkj.crm.billing.query.inventory.InventoryDetailQuery;
import com.hmkj.crm.billing.query.product.ProductBillQuery;
import com.hmkj.crm.billing.service.relevance.ProductInventoryDetailService;
import com.hmkj.crm.billing.service.relevance.RelProductInventoryService;
import com.hmkj.crm.billing.util.ExcelUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

/**
 * 库存 控制器
 *
 * @author sks.lwei
 * @date 2020/02/15
 */
@RestController
@RequestMapping("/repertory")
public class RepertoryController {

    @Resource
    private RelProductInventoryService productInventoryService;

    @Resource
    private ProductInventoryDetailService productInventoryDetailService;

    @RequestMapping("/listData")
    public Message listData(Page<RelProductInventory> page, RelProductInventory relProductInventory){
        String productUuid = relProductInventory.getProductUuid();
        String deliveryUuid = relProductInventory.getDeliveryUuid();
        QueryWrapper<RelProductInventory> wrapper = new QueryWrapper<>();
        if(StrUtil.isNotBlank(productUuid)){
            wrapper.eq("product_uuid", productUuid);
        }
        if(StrUtil.isNotBlank(deliveryUuid)){
            wrapper.eq("delivery_uuid", deliveryUuid);
        }
        IPage<ProductInventoryDTO> productInventoryPage = productInventoryService.listProductInventoryPage(page, wrapper);

        return Message.page(productInventoryPage);
    }

    /**
     * 库存产品详情（流水）
     * @param uuid relProductInventory uuid
     * @return {@link Message}
     */
    @RequestMapping("/detail")
    public Message productInventoryDetail(Page<ProductInventoryDetail> page, String uuid, @RequestParam(value = "type", required = false) String inventoryType){
        QueryWrapper<ProductInventoryDetail> wrapper = new QueryWrapper<>();
        wrapper.eq("inventory_uuid", uuid);
        if(StrUtil.isNotBlank(inventoryType)){
            wrapper.eq("inventory_type", inventoryType);
        }
        IPage<ProductInventoryDetailDTO> inventoryDetailPage = productInventoryDetailService.listProductInventoryDetailPage(page, wrapper);
        return Message.page(inventoryDetailPage);
    }

    @PostMapping("/export")
    public void export(HttpServletRequest request, HttpServletResponse response, String detail, RelProductInventory relProductInventory, String detailUuid, String inventoryType){
        String productUuid = relProductInventory.getProductUuid();
        String deliveryUuid = relProductInventory.getDeliveryUuid();
        Message message = null;
        String header = request.getHeader("export-type");
        QueryWrapper<RelProductInventory> wrapperInventory = new QueryWrapper<>();
        QueryWrapper<ProductInventoryDetail> wrapperInventoryDetail = new QueryWrapper<>();

        switch (header){
            case ExcelUtils.EXPORT_ALL:
                System.out.println("EXPORT_ALL");
                if(StrUtil.isNotBlank(productUuid)){
                    wrapperInventory.eq("product_uuid", productUuid);
                }
                if(StrUtil.isNotBlank(deliveryUuid)){
                    wrapperInventory.eq("delivery_uuid", deliveryUuid);
                }


                wrapperInventoryDetail.eq("inventory_uuid", detailUuid);
                if(StrUtil.isNotBlank(inventoryType)){
                    wrapperInventoryDetail.eq("inventory_type", inventoryType);
                }

                break;
            case ExcelUtils.EXPORT_SELECT:
                String[] ids = request.getParameterValues("ids");
                System.out.println("EXPORT_SELECT");
                wrapperInventory.in("id", (Object[]) ids);
                wrapperInventoryDetail.in("id", (Object[]) ids);
                break;
            default:
        }

        List<ExcelData> excelDataList = new LinkedList<>();

        List<? extends ExportComponent> exportDatas;
        if("detail".equals(detail)){
            exportDatas = productInventoryDetailService.export(wrapperInventoryDetail);
        }else {
            exportDatas = productInventoryService.export(wrapperInventory);
        }

        ExcelExportComponent exportComponent = ExcelExportComponent.build();
        ExcelData excelData = exportComponent.creatExcelData(exportDatas);

        excelDataList.add(excelData);
        try {
            ExcelUtils.exportExcel(response, "库存信息列表" + IdWorker.getMillisecond(), excelDataList);
            message = Message.SUCCESS;
        } catch (Exception e) {
            e.printStackTrace();
            message = Message.FAIL;
        }

        if(!message.isSuccess()){
            String jsonString = JSONUtils.toJSONString(message);
            try {
                response.getWriter().write(jsonString);
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }

    }

}
