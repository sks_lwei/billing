package com.hmkj.crm.billing.export;

import java.lang.annotation.*;

/**
 * 导出目标
 *
 * @author sks.lwei
 * @date 2020/03/02
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD})
public @interface ExportTarget {

    String title();

    int sort() default 0;

}
