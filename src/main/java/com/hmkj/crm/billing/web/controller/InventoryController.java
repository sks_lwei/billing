package com.hmkj.crm.billing.web.controller;

import com.alibaba.druid.support.json.JSONUtils;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.hmkj.crm.billing.domain.ProductBill;
import com.hmkj.crm.billing.domain.company.InventoryDetail;
import com.hmkj.crm.billing.dto.Message;
import com.hmkj.crm.billing.enums.AmountTypeEnum;
import com.hmkj.crm.billing.export.ExcelExportComponent;
import com.hmkj.crm.billing.export.ExportComponent;
import com.hmkj.crm.billing.export.domain.ExcelData;
import com.hmkj.crm.billing.query.AdvancedQueryComponent;
import com.hmkj.crm.billing.query.QueryComponent;
import com.hmkj.crm.billing.query.inventory.InventoryDetailQuery;
import com.hmkj.crm.billing.service.company.DeliveryCompaniesInfoService;
import com.hmkj.crm.billing.service.company.InventoryDetailService;
import com.hmkj.crm.billing.util.ExcelUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

/**
 * 公司账户（账单）
 *
 * 均以公司为中心进行计算
 *
 * @author sks.lwei
 * @date 2020/02/20
 */
@RestController
@RequestMapping("/inventory")
public class InventoryController {

    @Resource
    private InventoryDetailService inventoryDetailService;

    @RequestMapping("/listData")
    public Message listData(Page<InventoryDetail> page, InventoryDetailQuery query){
        AdvancedQueryComponent<InventoryDetail> component = new AdvancedQueryComponent<>();
        QueryWrapper<InventoryDetail> wrapper = component.buildQueryWrapper(query, InventoryDetail.class);
        IPage<InventoryDetail> inventoryDetail = inventoryDetailService.listInventoryDetailPage(page, wrapper);
        return Message.page(inventoryDetail);
    }

    /**
     * 资金变更记录
     *
     * @param amountTypeCode 类型代码
     * @param detail         细节
     * @return {@link Message}
     */
    @PostMapping("/moneyChange")
    public Message save(String amountTypeCode, InventoryDetail detail){

        if(detail.getAmount() == null || detail.getAmount() == 0 ){
            return Message.failMsg("无效的资金变更！");
        }

        //重置类型
        detail.setAmountType(AmountTypeEnum.getInstance(amountTypeCode));
        inventoryDetailService.addDetail(detail);
        return Message.SUCCESS;
    }

    @PostMapping("/export")
    public void export(HttpServletRequest request, HttpServletResponse response, InventoryDetailQuery inventoryDetailQuery){

        Message message = null;
        String header = request.getHeader("export-type");
        QueryWrapper<InventoryDetail> wrapper = null;

        switch (header){
            case ExcelUtils.EXPORT_ALL:
                System.out.println("EXPORT_ALL");
                AdvancedQueryComponent<InventoryDetail> component = new AdvancedQueryComponent<>();
                wrapper = component.buildQueryWrapper(inventoryDetailQuery, InventoryDetail.class);
                break;
            case ExcelUtils.EXPORT_SELECT:
                String[] ids = request.getParameterValues("ids");
                System.out.println("EXPORT_SELECT");
                wrapper = new QueryWrapper<>();
                wrapper.in("id", (Object[]) ids);
                break;
            default:
        }

        if(wrapper != null){
            List<ExcelData> excelDataList = new LinkedList<>();
            List<? extends ExportComponent> exportDatas = inventoryDetailService.export(wrapper);

            ExcelExportComponent exportComponent = ExcelExportComponent.build();
            ExcelData excelData = exportComponent.creatExcelData(exportDatas);

            excelDataList.add(excelData);
            try {
                ExcelUtils.exportExcel(response, "账户信息列表" + IdWorker.getMillisecond(), excelDataList);
                message = Message.SUCCESS;
            } catch (Exception e) {
                e.printStackTrace();
                message = Message.FAIL;
            }
        }else {
            message = Message.FAIL;
        }

        if(!message.isSuccess()){
            String jsonString = JSONUtils.toJSONString(message);
            try {
                response.getWriter().write(jsonString);
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }

    }

}
