package com.hmkj.crm.billing.domain.company;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.hmkj.crm.billing.domain.BaseDomain;
import com.hmkj.crm.billing.enums.AmountTypeEnum;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

/**
 * 账户明细
 *
 * @author sks.lwei
 * @date 2020/02/20
 */
@Getter
@Setter
@TableName("inventory_detail")
public class InventoryDetail extends BaseDomain<InventoryDetail> {

    /**
     * 订单号
     */
    @TableField("order_uuid")
    private String orderUuid;

    /**
     * 配送公司uuid
     */
    @TableField("company_uuid")
    private String companyUuid;

    /**
     * 配送公司名称
     */
    @TableField("company_name")
    private String companyName;

    /**
     * 金额类型（spending支出/income收入/receivable回款/poundage手续费/shipping配送费）
     */
    @TableField("amount_type")
    private AmountTypeEnum amountType;

    /**
     * 金额(根据amountType标识+/-)
     */
    @TableField
    private Double amount;

    /**
     * 实时余额
     */
    @TableField
    private Double balance;

    /**
     * 记账时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @TableField("bill_time")
    private Date billTime;

    /**
     * 备注
     */
    @TableField
    private String remark;
}
