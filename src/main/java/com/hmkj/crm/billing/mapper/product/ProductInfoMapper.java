package com.hmkj.crm.billing.mapper.product;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hmkj.crm.billing.domain.product.ProductInfo;

/**
 * 产品信息Mapper
 *
 * @author s.lwei
 * @date 2020/01/16
 */
public interface ProductInfoMapper extends BaseMapper<ProductInfo> {

}
