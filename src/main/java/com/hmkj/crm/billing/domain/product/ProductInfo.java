package com.hmkj.crm.billing.domain.product;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.hmkj.crm.billing.domain.BaseDomain;
import com.hmkj.crm.billing.dto.SelectItem;
import lombok.Getter;
import lombok.Setter;

/**
 * 产品信息
 *
 * 用于维护商品信息
 *
 * @author sks.lwei
 * @date 2020/01/16
 */
@Getter
@Setter
@TableName("product_info")
public class ProductInfo extends BaseDomain<ProductInfo> implements SelectItem {

    /**
     * 产品名称
     */
    @TableField("product_name")
    private String productName;

    /**
     * 供应商名称
     */
    @TableField("supplier_name")
    private String supplierName;
    private String supplierUuid;

    /**
     * 生产企业名称
     */
    @TableField("maker_name")
    private String makerName;
    private String makerUuid;

    /**
     * 规格
     */
    @TableField
    private String standard;

    /**
     * 单位
     */
    @TableField
    private String unit;

    /**
     * 件装单位
     */
    @TableField
    private Integer units;

    /**
     * 税率
     */
    @TableField
    private Double rate;

    /**
     * 销售价格(含税)
     */
    @TableField("sale_price")
    private Double salePrice;

    /**
     * 成本价格(含税进价)(含税)
     */
    @TableField("cost_price")
    private Double costPrice;

    /**
     * 返款单价
     */
    @TableField("return_price")
    private Double returnPrice;

    /**
     * 备注
     */
    private String remark;

    @Override
    public String getText() {
        return this.getProductName();
    }

    @Override
    public Object getVal() {
        return this.getUuid();
    }
}
