package com.hmkj.crm.billing.domain.relevance;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.hmkj.crm.billing.domain.BaseDomain;
import lombok.Getter;
import lombok.Setter;

/**
 * Rel产品库存关联表
 *
 * 关联产品及其库存情况
 *
 * @author sks.lwei
 * @date 2020/01/16
 */
@Getter
@Setter
@TableName("rel_product_inventory")
public class RelProductInventory extends BaseDomain<RelProductInventory> {

    /**
     * 产品uuid
     */
    @TableField("product_uuid")
    private String productUuid;

    /**
     * 采购来源公司 uuid
     */
    @TableField("delivery_uuid")
    private String deliveryUuid;

    /**
     * 库存数量
     */
    private Integer number;

    public RelProductInventory() {
    }

    public RelProductInventory(String productUuid, String deliveryUuid, Integer number) {
        this.productUuid = productUuid;
        this.deliveryUuid = deliveryUuid;
        this.number = number;
    }
}
