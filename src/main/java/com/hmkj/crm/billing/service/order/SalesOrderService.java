package com.hmkj.crm.billing.service.order;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.hmkj.crm.billing.domain.order.SalesOrder;
import com.hmkj.crm.billing.dto.SalesOrderDTO;
import com.hmkj.crm.billing.export.ExportComponent;

import java.util.List;

/**
 * 销售订单(出库)Service
 *
 * @author skswei
 * @date 2020/01/16
 */
public interface SalesOrderService extends IService<SalesOrder> {

    /**
     * 销售订单列表页面
     *
     * @param page    页面
     * @param wrapper 包装器
     * @return {@link IPage<SalesOrder>}
     */
    IPage<SalesOrderDTO> listSalesOrderPage(Page<SalesOrder> page, Wrapper<SalesOrder> wrapper);

    /**
     * 销售
     *
     * @param salesOrder   销售订单
     * @param deliveryUuid 配送公司Uuid（货源）
     */
    void sales(String deliveryUuid, SalesOrder salesOrder);

    /**
     * 销售撤销
     * 删除
     *
     * @param id   销售订单ID
     * @param mark 撤出原因
     */
    void unSales(Long id, String mark);

    /**
     * 通过uuid获取订单信息
     *
     * @param uuid uuid 订单号
     * @return {@link SalesOrder}
     */
    SalesOrder getSalesOrderByUuid(String uuid);

    /**
     * 导出
     *
     * @param wrapper 包装器
     * @return {@link List<? extends ExportComponent>}
     */
    List<? extends ExportComponent> export(QueryWrapper<SalesOrder> wrapper);

    /**
     * 删除
     *
     * @param id id
     */
    void del(Long id);
}
