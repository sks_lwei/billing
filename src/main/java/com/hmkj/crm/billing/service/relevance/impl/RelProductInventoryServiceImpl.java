package com.hmkj.crm.billing.service.relevance.impl;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hmkj.crm.billing.domain.order.Order;
import com.hmkj.crm.billing.domain.order.PurchaseOrder;
import com.hmkj.crm.billing.domain.product.ProductInfo;
import com.hmkj.crm.billing.domain.relevance.ProductInventoryDetail;
import com.hmkj.crm.billing.domain.relevance.RelProductInventory;
import com.hmkj.crm.billing.dto.ProductInventoryDTO;
import com.hmkj.crm.billing.dto.PurchasePriceDTO;
import com.hmkj.crm.billing.export.ExportComponent;
import com.hmkj.crm.billing.export.domain.ProductInventoryExport;
import com.hmkj.crm.billing.mapper.relevance.RelProductInventoryMapper;
import com.hmkj.crm.billing.service.company.DeliveryCompaniesInfoService;
import com.hmkj.crm.billing.service.order.PurchaseOrderService;
import com.hmkj.crm.billing.service.product.ProductInfoService;
import com.hmkj.crm.billing.service.relevance.ProductInventoryDetailService;
import com.hmkj.crm.billing.service.relevance.RelProductInventoryService;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Rel产品库存服务Impl
 *
 * @author sks.lwei
 * @date 2020/01/22
 */
@Log4j2
@Service
public class RelProductInventoryServiceImpl extends ServiceImpl<RelProductInventoryMapper, RelProductInventory> implements RelProductInventoryService {

    @Resource
    private DeliveryCompaniesInfoService deliveryCompaniesInfoService;

    @Resource
    private ProductInfoService productInfoService;

    /**
     * 购买订单服务
     */
    @Resource
    private PurchaseOrderService purchaseOrderService;

    /**
     * 产品库存关联表明细
     */
    @Resource
    private ProductInventoryDetailService productInventoryDetailService;

    @Override
    public IPage<ProductInventoryDTO> listProductInventoryPage(Page<RelProductInventory> page, Wrapper<RelProductInventory> wrapper) {
        Page<RelProductInventory> relProductInventoryPage = this.page(page, wrapper);
        return relProductInventoryPage.convert(this::convert);
    }

    private ProductInventoryDTO convert(RelProductInventory rel){
        ProductInventoryDTO inventoryDTO = new ProductInventoryDTO();
        BeanUtil.copyProperties(rel, inventoryDTO);
        //装载配送公司&产品 名称
        String deliveryCompaniesName = deliveryCompaniesInfoService.getDeliveryCompaniesNameByUuid(inventoryDTO.getDeliveryUuid());
        inventoryDTO.setDeliveryName(deliveryCompaniesName);
        //装载产品信息
        ProductInfo productInfo = productInfoService.getProductInfoByUuid(inventoryDTO.getProductUuid());
        inventoryDTO.buildProductInfo(productInfo);

        if(rel.getNumber() > 0){
            //装载采购信息
            PurchasePriceDTO purchaseInfo = purchaseOrderService.getPurchaseInfo(inventoryDTO.getProductUuid(), inventoryDTO.getDeliveryUuid());
            inventoryDTO.setTaxPaid(purchaseInfo.getTaxPaid());
            inventoryDTO.setNoTaxPaid(purchaseInfo.getNoTaxPaid());
            inventoryDTO.setTaxTotalPrice(inventoryDTO.getTaxPaid() * inventoryDTO.getNumber());
        }
        return inventoryDTO;
    }

    /**
     * 增加
     *
     * @param deliveryUuid 交付方uuid（货源）
     * @param order 采购订单
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public void up(String deliveryUuid, Order order) {
        //产品Uuid
        String productUuid = order.getProductUuid();
        //数量
        Integer number = order.getNumber();

        log.info("产品[{}], 增加库存[{}]件", productUuid, number);

        QueryWrapper<RelProductInventory> wrapper = new QueryWrapper<>();
        wrapper.eq("product_uuid", productUuid);
        wrapper.eq("delivery_uuid", deliveryUuid);
        RelProductInventory relProductInventory = this.getOne(wrapper);

        if(relProductInventory == null){
            log.info("[{}]新增库存[{}]件", productUuid, number);
            relProductInventory = new RelProductInventory(productUuid, deliveryUuid, number);
            relProductInventory.setUuid("relPI_"+ IdWorker.get32UUID());
            this.save(relProductInventory);
        }else {
            Integer storeNumber = relProductInventory.getNumber();
            storeNumber += number;
            UpdateWrapper<RelProductInventory> updateWrapper = new UpdateWrapper<>();
            updateWrapper.set("number", storeNumber);
            updateWrapper.eq("id", relProductInventory.getId());
            log.info("[{}]增加库存[{}]->[{}]", productUuid, relProductInventory.getNumber(), storeNumber);
            this.update(updateWrapper);
        }

        productInventoryDetailService.addDetail(relProductInventory.getUuid(), order);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void down(String deliveryUuid, Order order) {
        //产品Uuid
        String productUuid = order.getProductUuid();
        //数量
        Integer number = order.getNumber();
        log.info("产品[{}], 删减库存[{}]件", productUuid, number);

        QueryWrapper<RelProductInventory> wrapper = new QueryWrapper<>();
        wrapper.eq("product_uuid", productUuid);
        wrapper.eq("delivery_uuid", deliveryUuid);
        RelProductInventory relProductInventory = this.getOne(wrapper);

        if(relProductInventory == null){
            throw new NullPointerException("产品库存为空！无法减少！");
        }

        Integer storeNumber = relProductInventory.getNumber();
        storeNumber = storeNumber - number;

        if(storeNumber < 0){
            throw new IllegalArgumentException("库存不足！"+storeNumber);
        }

        Long id = relProductInventory.getId();
//        if(storeNumber == 0){
//            log.warn("库存减少为0，删除记录！记录id：[{}]", id);
//            this.removeById(id);
//        }else {
            log.info("[{}]减少库存[{}]->[{}]", productUuid, relProductInventory.getNumber(), storeNumber);
            UpdateWrapper<RelProductInventory> updateWrapper = new UpdateWrapper<>();
            updateWrapper.set("number", storeNumber);
            updateWrapper.eq("id", id);
            this.update(updateWrapper);
            log.info("更新库存成功");
//        }

        productInventoryDetailService.addDetail(relProductInventory.getUuid(), order);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void undo(String deliveryUuid, String productUuid, String orderUuid) {
        log.info("获取仓库记录，商品uuid：[{}], 商品配送公司uuid:[{}]", productUuid, deliveryUuid);
        QueryWrapper<RelProductInventory> wrapper = new QueryWrapper<>();
        wrapper.eq("product_uuid", productUuid).eq("delivery_uuid", deliveryUuid);
        RelProductInventory relProductInventory = this.getOne(wrapper);

        if(relProductInventory == null){
            log.warn("库存记录获取为空");
            throw new NullPointerException("库存记录获取为空");
        }

        log.info("删除产品库存关系记录");
        ProductInventoryDetail productInventoryDetail = productInventoryDetailService.undo(relProductInventory.getUuid(), orderUuid);
        Integer number = productInventoryDetail.getNumber();

        //恢复库存
        Integer currentNumber = relProductInventory.getNumber();

        currentNumber = currentNumber - number;

        Long id = relProductInventory.getId();
//        if(currentNumber == 0){
//            log.warn("库存减少为0，删除记录！记录id：[{}]", id);
//            this.removeById(id);
//        }else {
            log.warn("撤销产品库存关系记录，恢复库存[{}]->[{}]", relProductInventory.getNumber(), currentNumber);
            UpdateWrapper<RelProductInventory> update = new UpdateWrapper<>();
            update.set("number", currentNumber).eq("id", id);
            this.update(update);
//        }

    }

    @Override
    public List<? extends ExportComponent> export(QueryWrapper<RelProductInventory> wrapper) {
        List<RelProductInventory> list = this.list(wrapper);
        return list.stream().map(rel-> new ProductInventoryExport(convert(rel))).collect(Collectors.toList());
    }
}
