package com.hmkj.crm.billing.web.controller;

import com.alibaba.druid.support.json.JSONUtils;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.hmkj.crm.billing.domain.ProductBill;
import com.hmkj.crm.billing.domain.order.PurchaseOrder;
import com.hmkj.crm.billing.domain.order.ReturnOrder;
import com.hmkj.crm.billing.domain.order.SalesOrder;
import com.hmkj.crm.billing.dto.Message;
import com.hmkj.crm.billing.dto.SalesOrderDTO;
import com.hmkj.crm.billing.export.ExcelExportComponent;
import com.hmkj.crm.billing.export.ExportComponent;
import com.hmkj.crm.billing.export.domain.ExcelData;
import com.hmkj.crm.billing.query.AdvancedQueryComponent;
import com.hmkj.crm.billing.query.order.SalesOrderQuery;
import com.hmkj.crm.billing.service.ProductBillService;
import com.hmkj.crm.billing.service.order.PurchaseOrderService;
import com.hmkj.crm.billing.service.order.ReturnOrderService;
import com.hmkj.crm.billing.service.order.SalesOrderService;
import com.hmkj.crm.billing.util.ExcelUtils;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

/**
 * 订单控制器
 *
 * @author sks.lwei
 * @date 2020/02/18
 */
@RestController
@RequestMapping("/order")
public class OrderController {

    /**
     * 产品订单列表服务
     */
    @Resource
    private ProductBillService productBillService;

    /**
     * 采购订单服务
     */
    @Resource
    private PurchaseOrderService purchaseOrderService;

    /**
     * 销售订单服务
     */
    @Resource
    private SalesOrderService salesOrderService;

    /**
     * 退货订单服务
     */
    @Resource
    private ReturnOrderService returnOrderService;

    /* ---------------purchase---------------- */


    /**
     * 返款
     */
    @RequestMapping("/purchase/rebates")
    public Message rebates(@RequestParam("uuid") String purchaseOrderUuid, PurchaseOrder purchaseOrder){
        purchaseOrderService.rebates(purchaseOrderUuid, purchaseOrder);
        productBillService.rebates(purchaseOrderUuid, purchaseOrder);
        return Message.SUCCESS;
    }

    @GetMapping("/purchase/del")
    public Message purchaseDel(Long id){
        productBillService.del(id);
        return Message.SUCCESS;
    }

    /* -------------sales--------------- */

    /**
     * 销售
     */
    @PostMapping("/sales/add")
    public Message salesOrderAdd(String deliveryUuid, SalesOrder salesOrder){
        salesOrderService.sales(deliveryUuid, salesOrder);
        return Message.SUCCESS;
    }

    /**
     * 销售记录列表
     */
    @GetMapping("/sales/listData")
    public Message salesListData(Page<SalesOrder> page, SalesOrderQuery salesOrderQuery){
        AdvancedQueryComponent<SalesOrder> component = new AdvancedQueryComponent<>();
        QueryWrapper<SalesOrder> wrapper = component.buildQueryWrapper(salesOrderQuery, SalesOrder.class);
        IPage<SalesOrderDTO> orderPage = salesOrderService.listSalesOrderPage(page, wrapper);
        return Message.page(orderPage);
    }

    /**
     * 退货
     */
    @PostMapping("/sales/return")
    public Message salesReturn(String deliveryUuid, ReturnOrder returnOrder){
        returnOrderService.salesReturn(deliveryUuid, returnOrder);
        return Message.SUCCESS;
    }

    /**
     * 撤回
     */
    @GetMapping("/sales/undo")
    public Message toWithdraw(Long id, String mark){
        salesOrderService.unSales(id, mark);
        return Message.SUCCESS;
    }

    @GetMapping("/sales/del")
    public Message salesDel(Long id){
        salesOrderService.del(id);
        return Message.SUCCESS;
    }

    /**
     * 修改销售订单客户姓名
     */
    @PostMapping("/sales/editCName")
    public Message editSalesCustomerName(@RequestParam("u") String orderNumber, @RequestParam("n") String customerName){
        UpdateWrapper<SalesOrder> updateWrapper = new UpdateWrapper<>();
        updateWrapper.set("customer_name", customerName).eq("uuid", orderNumber);
        salesOrderService.update(updateWrapper);
        return Message.SUCCESS;
    }

    /**
     * 导出
     */
    @PostMapping("/sales/export")
    public void export(HttpServletRequest request, HttpServletResponse response, SalesOrderQuery salesOrderQuery){
        Message message = null;
        String header = request.getHeader("export-type");
        QueryWrapper<SalesOrder> wrapper = null;

        switch (header){
            case ExcelUtils.EXPORT_ALL:
                System.out.println("EXPORT_ALL");
                AdvancedQueryComponent<SalesOrder> component = new AdvancedQueryComponent<>();
                wrapper = component.buildQueryWrapper(salesOrderQuery, SalesOrder.class);
                break;
            case ExcelUtils.EXPORT_SELECT:
                String[] ids = request.getParameterValues("ids");
                System.out.println("EXPORT_SELECT");
                wrapper = new QueryWrapper<>();
                wrapper.in("id", (Object[]) ids);
                break;
            default:
        }

        if(wrapper != null){
            List<ExcelData> excelDataList = new LinkedList<>();
            List<? extends ExportComponent> exportDatas = salesOrderService.export(wrapper);

            ExcelExportComponent exportComponent = ExcelExportComponent.build();
            ExcelData excelData = exportComponent.creatExcelData(exportDatas);

            excelDataList.add(excelData);
            try {
                ExcelUtils.exportExcel(response, "销售信息列表" + IdWorker.getMillisecond(), excelDataList);
                message = Message.SUCCESS;
            } catch (Exception e) {
                e.printStackTrace();
                message = Message.FAIL;
            }
        }else {
            message = Message.FAIL;
        }

        if(!message.isSuccess()){
            String jsonString = JSONUtils.toJSONString(message);
            try {
                response.getWriter().write(jsonString);
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }

    }


}
