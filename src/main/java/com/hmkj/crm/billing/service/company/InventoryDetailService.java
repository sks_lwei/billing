package com.hmkj.crm.billing.service.company;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.hmkj.crm.billing.domain.company.InventoryDetail;
import com.hmkj.crm.billing.enums.AmountTypeEnum;
import com.hmkj.crm.billing.export.ExportComponent;

import java.util.List;

/**
 * 公司账户，账单 服务
 *
 * @author sks.lwei
 * @date 2020/02/20
 */
public interface InventoryDetailService extends IService<InventoryDetail> {

    /**
     * 获取 公司账户账单 列表页面
     *
     * @param page    页面
     * @param wrapper 包装器
     * @return {@link IPage< InventoryDetail >}
     */
    IPage<InventoryDetail> listInventoryDetailPage(Page<InventoryDetail> page, Wrapper<InventoryDetail> wrapper);

    /**
     * 添加
     *
     * @param inventoryDetail 交货清单细节
     */
    void addDetail(InventoryDetail inventoryDetail);

    /**
     * 资金变更
     * 采购/手续费
     *
     * @param orderUuid   订单Uuid
     * @param companyUuid 公司Uuid
     * @param amount      金额
     * @param amountType 资金类型
     * @param mark 变更备注
     */
    void accountChange(String orderUuid, String companyUuid, Double amount, AmountTypeEnum amountType, String mark);

    /**
     * 撤销账户变化
     * (删除记录)
     *
     * @param orderUuid   订单Uuid
     * @param companyUuid 公司Uuid
     * @param amountType  指定 金额类型
     */
    void undoAccountChange(String orderUuid, String companyUuid, AmountTypeEnum amountType);

    /**
     * 导出
     *
     * @param wrapper 包装器
     * @return {@link List<? extends ExportComponent>}
     */
    List<? extends ExportComponent> export(QueryWrapper<InventoryDetail> wrapper);
}
