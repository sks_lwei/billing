package com.hmkj.crm.billing.export.domain;

import cn.hutool.core.bean.BeanUtil;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.hmkj.crm.billing.domain.ProductBill;
import com.hmkj.crm.billing.domain.product.ProductInfo;
import com.hmkj.crm.billing.export.ExportTarget;

import java.util.Date;

/**
 * ProductBill导出对象
 *
 * @author sks.lwei
 * @date 2020/03/03
 */
public class ProductBillExport extends ProductExport {

    /**
     * 订单号
     */
    @ExportTarget(title = "订单号", sort = 100)
    public String purchaseOder;

    /**
     * 配送公司
     */
    @ExportTarget(title = "配送公司")
    public String deliveryName;

    /**
     * 销售价格
     */
    @ExportTarget(title = "销售价格")
    public Double salePrice;

    /**
     * 成本价格(含税进价)
     */
    @ExportTarget(title = "含税进价")
    public Double costPrice;

    /**
     * 订单时间
     */
    @ExportTarget(title = "订单时间")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm", timezone = "GMT+8")
    public Date orderTime;

    /**
     * 数量
     */
    @ExportTarget(title = "数量")
    public Integer number;

    /**
     * 含税总金额
     */
    @ExportTarget(title = "含税总金额")
    public Double paymentAmount;

    /**
     * 手续费
     */
    @ExportTarget(title = "手续费")
    public Double poundage;

    /**
     * 是否到达
     */
    @ExportTarget(title = "是否到达")
    public Boolean arrive;

    /**
     * 入库时间
     */
    @ExportTarget(title = "入库时间")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm", timezone = "GMT+8")
    public Date arriveTime;

    /**
     * 是否返款
     */
    @ExportTarget(title = "是否返款")
    public Boolean rebates;

    /**
     * 返款时间
     */
    @ExportTarget(title = "返款时间")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm", timezone = "GMT+8")
    public Date rebatesTime;

    /**
     * 备注
     */
    @ExportTarget(title = "备注")
    public String remark;

    public ProductBillExport(ProductInfo productInfo, ProductBill productBill) {
        BeanUtil.copyProperties(productInfo, this);
        BeanUtil.copyProperties(productBill, this);
    }
}
