package com.hmkj.crm.billing.domain.company;


import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.hmkj.crm.billing.domain.BaseDomain;
import com.hmkj.crm.billing.dto.SelectItem;
import lombok.Getter;
import lombok.Setter;

/**
 * 配送公司信息
 *
 * @author sks.lwei
 * @date 2020/01/16
 */
@Getter
@Setter
@TableName("delivery_companies_info")
public class DeliveryCompaniesInfo extends BaseDomain<DeliveryCompaniesInfo> implements SelectItem {

    /**
     * 全名
     */
    @TableField("full_name")
    private String fullName;

    /**
     * 备注
     */
    @TableField
    private String remark;

    /**
     * 账户余额
     */
    @TableField
    private Double balance;

    @Override
    public String getText() {
        return getFullName();
    }

    @Override
    public Object getVal() {
        return getUuid();
    }
}
