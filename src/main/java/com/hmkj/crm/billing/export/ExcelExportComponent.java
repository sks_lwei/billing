package com.hmkj.crm.billing.export;

import cn.hutool.core.date.DateUtil;
import com.hmkj.crm.billing.export.domain.ExcelData;
import lombok.Getter;

import java.lang.reflect.Field;
import java.text.DateFormat;
import java.util.*;

/**
 * Excel导出组件
 *
 * @author sks.lwei
 * @date 2020/03/03
 */
public class ExcelExportComponent {

    @Getter
    private ExportComponent component;

    /**
     * 表头
     */
    private List<String> titles = new ArrayList<>();

    /**
     * 数据
     */
    private List<List<Object>> rows = new LinkedList<>();

    public ExcelExportComponent() {
    }

    public ExcelExportComponent(ExportComponent component){
        this.component = component;
    }

    public static ExcelExportComponent build(){
        return new ExcelExportComponent();
    }


    /**
     * 创造Excel数据
     *
     * @param components 组件
     * @return {@link ExcelData}
     */
    public ExcelData creatExcelData(List<? extends ExportComponent> components){
        for (ExportComponent exportComponent : components) {
            List<Object> row = new ArrayList<>();
            Field[] fields = exportComponent.getClass().getFields();
            List<Field> fieldList = Arrays.asList(fields);

            //fields排序
            fieldList.sort((o1, o2) -> {
                ExportTarget t1 = o1.getAnnotation(ExportTarget.class);
                ExportTarget t2 = o2.getAnnotation(ExportTarget.class);

                if(t1 == null || t2 == null){
                    return 0;
                }

                if(t1.sort() > t2.sort()){
                    return t2.sort() - t1.sort();
                }else {
                    return 0;
                }
            });

            for (Field field : fieldList) {

                ExportTarget exportTarget = field.getAnnotation(ExportTarget.class);
                if(exportTarget == null){
                    continue;
                }

                String title = exportTarget.title();
                int local = checkTitles(title);

                Object val = "";
                try {
                    val = field.get(exportComponent);

                    //处理特殊数据类型
                    Class<?> type = field.getType();
                    if(Boolean.class.equals(type)){
                        val = (boolean)val ? "是":"否";
                    }else if(Date.class.equals(type)){
                        val = DateUtil.format((Date)val, DateFormat.getDateTimeInstance());
                    }


                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
                row.add(local, val);
            }
            rows.add(row);
        }

        ExcelData excelData = new ExcelData();

        excelData.setTitles(titles);
        excelData.setRows(rows);
        excelData.setName("标签页");
        return excelData;
    }


    /**
     * 检查标题位置
     * 确定数据存放位置
     *
     * @param title 标题
     * @return int
     */
    private int checkTitles(String title){
        if(!titles.contains(title)){
            int local = titles.size();
            titles.add(local, title);
            return local;
        }else {
            return titles.indexOf(title);
        }
    }




}
