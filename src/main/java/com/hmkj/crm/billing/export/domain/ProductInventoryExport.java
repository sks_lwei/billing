package com.hmkj.crm.billing.export.domain;

import cn.hutool.core.bean.BeanUtil;
import com.hmkj.crm.billing.domain.product.ProductInfo;
import com.hmkj.crm.billing.dto.ProductInventoryDTO;
import com.hmkj.crm.billing.export.ExportComponent;
import com.hmkj.crm.billing.export.ExportTarget;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

/**
 * 产品库存 导出
 *
 * @author sks.lwei
 * @date 2020/02/15
 */
@Getter
@Setter
@ToString
public class ProductInventoryExport extends ProductExport {

    /**
     * 采购来源公司
     */
    @ExportTarget(title = "配送公司")
    public String deliveryName;

    /**
     * 库存数量
     */
    @ExportTarget(title = "库存数量")
    public Integer number;

    //采购信息

    /**
     * 含税进价(采购单价)
     */
    @ExportTarget(title = "含税进价")
    public Double taxPaid;

    /**
     * 不含税进价
     */
    @ExportTarget(title = "不含税进价")
    public Double noTaxPaid;

    /**
     * 含税总价格
     */
    @ExportTarget(title = "含税总金额")
    public Double taxTotalPrice;

    public ProductInventoryExport(ProductInventoryDTO dto){
        BeanUtil.copyProperties(dto, this);
    }

}
