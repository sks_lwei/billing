package com.hmkj.crm.billing;

import cn.hutool.core.text.StrSpliter;
import com.hmkj.crm.billing.query.QueryComponent;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
class BillingApplicationTests {

    @Test
    void contextLoads() {
    }

    public static void main(String[] args) {
        String fieldValue = "a   b  ~1";

        List<String> items = StrSpliter.split(fieldValue.toString(), QueryComponent.BETWEEN, 2, true, true);
        System.out.println(items.size());
        for (String item : items) {
            System.out.println(item);
        }
    }

}
