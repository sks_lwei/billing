package com.hmkj.crm.billing.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hmkj.crm.billing.domain.ProductBill;

/**
 * ProductBill映射
 *
 * @author sks.lwei
 * @date 2020/01/16
 */
public interface ProductBillMapper extends BaseMapper<ProductBill> {
}
