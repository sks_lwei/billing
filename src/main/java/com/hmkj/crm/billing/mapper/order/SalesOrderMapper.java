package com.hmkj.crm.billing.mapper.order;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hmkj.crm.billing.domain.order.SalesOrder;

/**
 * 销售订单(出库)Mapper
 *
 * @author skswei
 * @date 2020/01/16
 */
public interface SalesOrderMapper extends BaseMapper<SalesOrder> {

}
