package com.hmkj.crm.billing.export.domain;

import com.hmkj.crm.billing.enums.InventoryTypeEnum;
import com.hmkj.crm.billing.export.ExportTarget;

/**
 * 产品库存详细 导出
 *
 * @author sks.lwei
 * @date 2020/03/03
 */
public class ProductInventoryDetailExport extends ProductExport{

    /**
     * 订单号（入库订单号 出库订单号 ）
     */
    @ExportTarget(title = "订单号")
    public String orderUuid;

    /**
     * 库存类型
     */
    @ExportTarget(title = "变更状态")
    public String inventoryType;

    /**
     * 变更数量 （正负表示）
     */
    @ExportTarget(title = "变更数量")
    public Integer number;

    /**
     * 含税进/销价(单价)
     */
    @ExportTarget(title = "含税进/销价")
    public Double price;

    /**
     * 含税总金额(付款额)
     */
    @ExportTarget(title = "含税总金额")
    public Double paymentAmount;

    /**
     * 不含税进/销价
     */
    @ExportTarget(title = "不含税进/销价")
    public Double noTaxPaid;

    /**
     * 不含税总金额
     */
    @ExportTarget(title = "不含税总金额")
    public Double noTaxAmount;

    /**
     * 返款价格
     */
    @ExportTarget(title = "返款单价")
    public Double returnPrice;

    /**
     * 返款总价格
     */
    @ExportTarget(title = "返款总金额")
    public Double returnTotalPrice;

    /**
     * 实收金额
     */
    @ExportTarget(title = "实收金额")
    public Double actualPrice;

}
