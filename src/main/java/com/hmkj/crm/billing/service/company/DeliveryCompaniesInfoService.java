package com.hmkj.crm.billing.service.company;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.hmkj.crm.billing.domain.company.DeliveryCompaniesInfo;
import com.hmkj.crm.billing.service.CommonComponentService;

import java.util.List;

/**
 * 配送公司信息 Service
 *
 * @author sks.lwei
 * @date 2020/01/16
 */
public interface DeliveryCompaniesInfoService extends CommonComponentService<DeliveryCompaniesInfo> {

    /**
     * 配送公司信息页
     * 配送公司信息页
     *
     * @param page                  页面
     * @param deliveryCompaniesInfo 配送公司信息
     * @return {@link IPage<DeliveryCompaniesInfo>}
     */
    IPage<DeliveryCompaniesInfo> pageDeliveryCompaniesInfo(Page<DeliveryCompaniesInfo> page, DeliveryCompaniesInfo deliveryCompaniesInfo);

    /**
     * 配送公司信息列表
     *
     * @param deliveryCompaniesInfo 配送公司信息
     * @return {@link List<DeliveryCompaniesInfo>}
     */
    List<DeliveryCompaniesInfo> listDeliveryCompaniesInfo(DeliveryCompaniesInfo deliveryCompaniesInfo);

    /**
     * 得到配送公司的信息
     *
     * @param id id
     * @return {@link DeliveryCompaniesInfo}
     */
    DeliveryCompaniesInfo getDeliveryCompaniesInfo(Long id);

    /**
     * 配送公司信息Byuuid
     *
     * @param uuid uuid
     * @return {@link DeliveryCompaniesInfo}
     */
    DeliveryCompaniesInfo getDeliveryCompaniesInfoByuuid(String uuid);

    /**
     * 配送公司名称Byuuid
     *
     * @param uuid uuid
     * @return {@link String}
     */
    String getDeliveryCompaniesNameByUuid(String uuid);

    /**
     * 节省配送公司信息
     *
     * @param deliveryCompaniesInfo 配送公司信息
     */
    void saveDeliveryCompaniesInfo(DeliveryCompaniesInfo deliveryCompaniesInfo);

    /**
     * 编辑配送公司信息
     *
     * @param id                    id
     * @param deliveryCompaniesInfo 配送公司信息
     */
    void editDeliveryCompaniesInfo(Long id, DeliveryCompaniesInfo deliveryCompaniesInfo);

    /**
     * 配送公司信息去掉
     *
     * @param id id
     */
    void removeDeliveryCompaniesInfo(Long id);

    /**
     * 获取配送公司账户金额
     *
     * @param uuid deliveryCompaniesUuid
     * @return {@link Double}
     */
    Double getDeliveryBalance(String uuid);

    /**
     * 更新配送公司账户金额
     *
     * @param uuid    uuid
     * @param balance 账户实时金额
     */
    void setDeliveryBalance(String uuid, Double balance);
}
