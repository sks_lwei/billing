package com.hmkj.crm.billing.service.order;

import com.baomidou.mybatisplus.extension.service.IService;
import com.hmkj.crm.billing.domain.order.ReturnOrder;

/**
 * 返回订单服务
 *
 * @author sks.lwei
 * @date 2020/02/26
 */
public interface ReturnOrderService extends IService<ReturnOrder> {

    /**
     * 销货退回
     *
     * @param deliveryUuid 配送公司Uuid（货源）
     * @param returnOrder  返回订单
     */
    void salesReturn(String deliveryUuid, ReturnOrder returnOrder);

    /**
     * 得到返款订单 By Uuid
     *
     * @param uuid uuid
     * @return {@link ReturnOrder}
     */
    ReturnOrder getReturnOrderByUuid(String uuid);
}
