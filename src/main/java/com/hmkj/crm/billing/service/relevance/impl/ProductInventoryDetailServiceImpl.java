package com.hmkj.crm.billing.service.relevance.impl;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hmkj.crm.billing.domain.order.Order;
import com.hmkj.crm.billing.domain.order.PurchaseOrder;
import com.hmkj.crm.billing.domain.order.ReturnOrder;
import com.hmkj.crm.billing.domain.order.SalesOrder;
import com.hmkj.crm.billing.domain.product.ProductInfo;
import com.hmkj.crm.billing.domain.relevance.ProductInventoryDetail;
import com.hmkj.crm.billing.dto.ProductInventoryDetailDTO;
import com.hmkj.crm.billing.enums.InventoryTypeEnum;
import com.hmkj.crm.billing.export.ExportComponent;
import com.hmkj.crm.billing.export.domain.ProductInventoryDetailExport;
import com.hmkj.crm.billing.mapper.relevance.ProductInventoryDetailMapper;
import com.hmkj.crm.billing.service.order.PurchaseOrderService;
import com.hmkj.crm.billing.service.order.ReturnOrderService;
import com.hmkj.crm.billing.service.order.SalesOrderService;
import com.hmkj.crm.billing.service.product.ProductInfoService;
import com.hmkj.crm.billing.service.relevance.ProductInventoryDetailService;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * 产品库存关联表明细 服务shix
 *
 * @author sks.lwei
 * @date 2020/02/15
 */
@Log4j2
@Service
public class ProductInventoryDetailServiceImpl extends ServiceImpl<ProductInventoryDetailMapper, ProductInventoryDetail> implements ProductInventoryDetailService {

    /**
     * 订单服务
     */
    @Resource
    private PurchaseOrderService purchaseOrderService;

    /**
     * 销售订单服务
     */
    @Resource
    private SalesOrderService salesOrderService;

    /**
     * 退货订单服务
     */
    @Resource
    private ReturnOrderService returnOrderService;

    /**
     * 产品信息服务
     */
    @Resource
    private ProductInfoService productInfoService;

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void addDetail(String relProductInventoryUuid, Order order) {
        ProductInventoryDetail detail = new ProductInventoryDetail();
        detail.setUuid("pid_"+ IdWorker.get32UUID());
        detail.setInventoryUuid(relProductInventoryUuid);
        InventoryTypeEnum orderInventoryType = order.getOrderInventoryType();
        Integer number = order.getNumber();
        if(orderInventoryType.getType() < 0){
            number = -number;
        }
        detail.setNumber(number);
        detail.setOrderUuid(order.getOrderUuid());
        detail.setTotalPrice(order.getTotalPrice());
        detail.setInventoryType(orderInventoryType);
        this.save(detail);
    }

    @Override
    public IPage<ProductInventoryDetailDTO> listProductInventoryDetailPage(Page<ProductInventoryDetail> page, Wrapper<ProductInventoryDetail> wrapper) {
        Page<ProductInventoryDetail> inventoryDetailPage = this.page(page, wrapper);
        return inventoryDetailPage.convert(this::convertDTO);
    }

    @Override
    public ProductInventoryDetail undo(String inventoryUuid, String orderNumber) {
        log.info("删除产品仓库记录关系，订单号[{}], 仓库uuid：[{}]", orderNumber, inventoryUuid);
        QueryWrapper<ProductInventoryDetail> wrapper = new QueryWrapper<>();
        wrapper.eq("order_uuid", orderNumber).eq("inventory_uuid", inventoryUuid);
        ProductInventoryDetail productInventoryDetail = this.getOne(wrapper);

        if(productInventoryDetail == null){
            throw new NullPointerException("无库存记录，删除失败");
        }

        this.removeById(productInventoryDetail.getId());
        return productInventoryDetail;
    }

    @Override
    public List<? extends ExportComponent> export(QueryWrapper<ProductInventoryDetail> wrapper) {
        List<ProductInventoryDetail> productInventoryDetails = this.list(wrapper);
        return productInventoryDetails.stream().map(this::convertExport).collect(Collectors.toList());
    }

    private ProductInventoryDetailDTO convertDTO(ProductInventoryDetail inventoryDetail){
        ProductInventoryDetailDTO dto = new ProductInventoryDetailDTO(inventoryDetail);

        InventoryTypeEnum inventoryType = inventoryDetail.getInventoryType();
        String orderUuid = inventoryDetail.getOrderUuid();

        switch (inventoryType){
            case INCOME:
                //进货订单
                PurchaseOrder purchaseOrder = purchaseOrderService.getPurchaseOrderByUuid(orderUuid);
                BeanUtil.copyProperties(purchaseOrder, dto);
                break;
            case SPENDING:
                //销售订单
                SalesOrder salesOrder = salesOrderService.getSalesOrderByUuid(orderUuid);
//                    BeanUtil.copyProperties(salesOrder, dto);
                dto = dto.toSalesOrder(salesOrder);
//                    dto.setNumber(inventoryDetail.getNumber());
                break;
            case RETURN:
                //退货订单
                ReturnOrder returnOrder = returnOrderService.getReturnOrderByUuid(orderUuid);
                dto = dto.toReturnOrder(returnOrder);
                break;
            default:
        }
        dto.setId(inventoryDetail.getId());
        return dto;
    }

    private ProductInventoryDetailExport convertExport(ProductInventoryDetail inventoryDetail){
        ProductInventoryDetailExport detailExport = new ProductInventoryDetailExport();
        ProductInventoryDetailDTO dto = new ProductInventoryDetailDTO(inventoryDetail);

        InventoryTypeEnum inventoryType = inventoryDetail.getInventoryType();
        String orderUuid = inventoryDetail.getOrderUuid();

        String productUuid = null;
        switch (inventoryType){
            case INCOME:
                //进货订单
                PurchaseOrder purchaseOrder = purchaseOrderService.getPurchaseOrderByUuid(orderUuid);
                BeanUtil.copyProperties(purchaseOrder, detailExport);
                productUuid = purchaseOrder.getProductUuid();

                break;
            case SPENDING:
                //销售订单
                SalesOrder salesOrder = salesOrderService.getSalesOrderByUuid(orderUuid);
                BeanUtil.copyProperties(dto.toSalesOrder(salesOrder), detailExport);
                productUuid = salesOrder.getProductUuid();
                break;
            case RETURN:
                //退货订单
                ReturnOrder returnOrder = returnOrderService.getReturnOrderByUuid(orderUuid);
                BeanUtil.copyProperties(dto.toReturnOrder(returnOrder), detailExport);
                productUuid = returnOrder.getProductUuid();
                break;
            default:
        }

        detailExport.inventoryType = inventoryType.getTitle();

        ProductInfo productInfo = productInfoService.getProductInfoByUuid(productUuid);
        BeanUtil.copyProperties(productInfo, detailExport);

        return detailExport;
    }

}
