package com.hmkj.crm.billing.domain.relevance;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.hmkj.crm.billing.domain.BaseDomain;
import com.hmkj.crm.billing.enums.InventoryTypeEnum;
import lombok.Getter;
import lombok.Setter;

/**
 * 产品库存关联表明细
 *
 * @author sks.lwei
 * @date 2020/02/15
 */
@Getter
@Setter
@TableName("product_inventory_detail")
public class ProductInventoryDetail extends BaseDomain<ProductInventoryDetail> {

    /**
     * 订单号（入库订单号 出库订单号 ）
     */
    @TableField("order_uuid")
    private String orderUuid;

    /**
     * Rel产品库存关联表uuid
     */
    @TableField("inventory_uuid")
    private String inventoryUuid;

    /**
     * 库存类型
     */
    @TableField("inventory_type")
    private InventoryTypeEnum inventoryType;

    /**
     * 变更数量 （正负表示）
     */
    @TableField("number")
    private Integer number;

    /**
     * 总价格
     */
    @TableField("total_price")
    private Double totalPrice;

}
