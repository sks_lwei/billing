package com.hmkj.crm.billing.mapper.order;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hmkj.crm.billing.domain.order.PurchaseOrder;

/**
 * 采购订单(入库)Mapper
 *
 * @author sks * @date 2020/01/16
 */
public interface PurchaseOrderMapper extends BaseMapper<PurchaseOrder> {
}
