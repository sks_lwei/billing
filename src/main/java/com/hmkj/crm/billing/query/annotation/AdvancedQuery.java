package com.hmkj.crm.billing.query.annotation;

import com.hmkj.crm.billing.query.enums.QueryMethodEnum;

import java.lang.annotation.*;

/**
 * 查询组件
 *
 * @author sks.lwei
 * @date 2020/02/27
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD})
public @interface AdvancedQuery {

    QueryMethodEnum method();

    String tableField() default "";

}
