package com.hmkj.crm.billing.mapper.company;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hmkj.crm.billing.domain.company.DeliveryCompaniesInfo;

/**
 * 配送公司信息 Mapper
 *
 * @author sks.lwei
 * @date 2020/01/16
 */
public interface DeliveryCompaniesInfoMapper extends BaseMapper<DeliveryCompaniesInfo> {



}
