package com.hmkj.crm.billing.mapper.relevance;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hmkj.crm.billing.domain.relevance.RelProductInventory;

/**
 * Rel产品库存映射器
 *
 * @author sks.lwei
 * @date 2020/01/22
 */
public interface RelProductInventoryMapper extends BaseMapper<RelProductInventory> {
}
