package com.hmkj.crm.billing.dto;

import com.hmkj.crm.billing.domain.order.PurchaseOrder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * 购买价格DTO
 * 记录购买价格
 *
 * @author sks.lwei
 * @date 2020/03/05
 */
@Getter
@Setter
@ToString
public class PurchasePriceDTO {


    /**
     * 含税进价(采购单价)
     */
    private Double taxPaid;

    /**
     * 不含税进价
     */
    private Double noTaxPaid;

    public PurchasePriceDTO(PurchaseOrder purchaseOrder) {
        this.taxPaid = purchaseOrder.getPrice();
        this.noTaxPaid = purchaseOrder.getNoTaxPaid();
    }

    public PurchasePriceDTO(Double taxPaid, Double noTaxPaid) {
        this.taxPaid = taxPaid;
        this.noTaxPaid = noTaxPaid;
    }
}
