package com.hmkj.crm.billing.service.order.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hmkj.crm.billing.domain.order.ReturnOrder;
import com.hmkj.crm.billing.mapper.order.ReturnOrderMapper;
import com.hmkj.crm.billing.service.order.ReturnOrderService;
import com.hmkj.crm.billing.service.relevance.RelProductInventoryService;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * 退款订单 服务Impl
 *
 * @author sks.lwei
 * @date 2020/02/26
 */
@Log4j2
@Service
public class ReturnOrderServiceImpl extends ServiceImpl<ReturnOrderMapper, ReturnOrder> implements ReturnOrderService {

    /**
     * rel产品库存服务
     */
    @Resource
    private RelProductInventoryService productInventoryService;

    @Override
    public void salesReturn(String deliveryUuid, ReturnOrder returnOrder) {
        log.info("退货, 订单uuid[{}]", returnOrder.getUuid());

        returnOrder.setUuid("R"+ IdWorker.getMillisecond());
        this.save(returnOrder);

        //削减库存
        productInventoryService.down(deliveryUuid, returnOrder);
        log.info("退货订单出库完成");

    }

    @Override
    public ReturnOrder getReturnOrderByUuid(String uuid) {
        QueryWrapper<ReturnOrder> wrapper = new QueryWrapper<>();
        wrapper.eq("uuid", uuid);
        return this.getOne(wrapper);
    }
}
